import React from 'react';
import { Row, Col, Card, List, Rate, Button } from 'antd';
import { useSelector } from 'react-redux';
import SearchQuiz from './SearchQuiz';
import FilterTypeQuiz from './FilterTypeQuiz';
import { useHistory } from 'react-router';

const { Meta } = Card;

const ListQuiz = () => {
    const quiz = useSelector(state => state.quizs.quiz);
    const filter = useSelector(state => state.home.filter);
    const text = useSelector(state => state.home.searchText);
    const history = useHistory();

    let renderData = quiz;
    renderData = filter === -1 ?
        renderData
        :
        renderData.filter(data => data.types.includes(filter));

    renderData = text === '' ?
        renderData
        :
        renderData.filter(data => (data.title.match(text)));

    return (
        <div style={{ background: "#F39C12", paddingBottom: 30 }}>
            <Row type="flex" justify="center">
                <Col style={{ margin: "30px 0px 20px 0px" }}>
                    <SearchQuiz />
                    <FilterTypeQuiz />
                </Col>
                <Col span={20}>
                    <Row type="flex" justify="center">
                        {renderData.map((item, index) => {
                            return (
                                <Col>
                                    <Card
                                        hoverable
                                        style={{ borderRadius: 10, width: 300, margin: 10 }}
                                        cover={<img src={item.image} alt="coverImg" style={{ height: 150, borderTopLeftRadius: 10, borderTopRightRadius: 10 }} />}
                                        key={index}
                                    >
                                        <div style={{ padding: 15 }}>
                                            <Meta title={item.title} description={item.description} />
                                            <Row type="flex" justify="center" style={{ padding: "20px 0px 10px 0px" }}>
                                                <Col>
                                                    <Rate disabled defaultValue={item.rating} />
                                                </Col>
                                            </Row>
                                            <Row type="flex" justify="center">
                                                <Col>
                                                    <Button type="danger" onClick={() => history.push("/doingQuiz/" + item._id)}>Start</Button>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Card>
                                </Col>
                            )
                        })}
                    </Row>
                </Col>
            </Row>
        </div>
    );
}

export default ListQuiz;