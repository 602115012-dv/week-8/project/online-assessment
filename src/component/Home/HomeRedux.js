import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import UserInfo from './UserInfo';
import ListQuiz from './ListQuiz';
import { fetchUser } from '../../reducers/SetUserReducer';
import { fetchQuiz } from '../../reducers/SetQuizReducer';
import HeaderPage from '../../components/HeaderPage';

const HomePage = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUser());
        dispatch(fetchQuiz());
    }, [])

    return (
        <div>
            <HeaderPage />
            <div style={{ background: "#F39C12", minHeight: "100vh", paddingTop: "66px" }}>
                <UserInfo />
                <ListQuiz />
            </div>
        </div>
    );
}

export default HomePage;
