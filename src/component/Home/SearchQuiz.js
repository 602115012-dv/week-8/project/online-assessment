import React from 'react'
import { useDispatch } from 'react-redux';
import { Input } from 'antd';
import { searchText } from '../../reducers/HomeReducer';

const { Search } = Input;

const SearchQuiz = () => {
    const dispatch = useDispatch()

    const setSearchText = (text) => {
        dispatch(searchText(text))
    }

    return (
        <Search
            placeholder="input search text"
            onChange={(e) => setSearchText(e.target.value)}
            style={{
                width: 500
            }}
        />
    );
}

export default SearchQuiz;
