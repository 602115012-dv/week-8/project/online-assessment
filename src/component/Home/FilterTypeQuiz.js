import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Select } from 'antd';
import { filterType } from '../../reducers/HomeReducer';
const { Option } = Select;

const FilterTypeQuiz = () => {
    const dispatch = useDispatch();

    const quiz = useSelector(state => state.quizs.quiz)

    const handleChange = (value) => {
        dispatch(filterType(value))
    }

    const [filter, setFilter] = useState([])

    let filterItems = filter;
    quiz.map((dataitem) => {
        return dataitem.types.map(item => {
            if (!filterItems.find(x => x == item)) {
                filterItems.push(item)
            }
        })
    })



    console.log(quiz)

    return (
        <Select defaultValue="All" style={{ width: 120, marginLeft: 40 }} onChange={handleChange}>
            <Option value={-1}>All</Option>
            {

                filterItems.map((item, index) => {
                    //alert(item)
                    return (
                        <Option value={item} key={index}>{item}</Option>
                    )
                })

            }

        </Select>
    );
}

export default FilterTypeQuiz;
