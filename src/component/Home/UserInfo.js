import React from 'react';
import { Typography, Row, Col, Button } from 'antd';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';

const { Title } = Typography;

const UserInfo = () => {
    const user = useSelector(state => state.checklogin.user);
    const history = useHistory();
    return (
        <div style={{ background: "#FFFFFF", minHeight: 480, textAlign: "center" }}>
            <Row>
                <Button style={{ float: 'right', margin: '2em 2em', backgroundColor: '#FFD494' }} icon="plus" onClick={() => history.push('/quiz/create')}><b> Create new Quiz</b></Button>
            </Row>
            <Title style={{ textAlign: "center" }}>QUIZ!!</Title>
            {
                user &&
                <img
                    src={user.image}
                    alt="userImg"
                    style={{
                        marginTop: 10,
                        width: 230,
                        height: 230,
                        borderRadius: "50%"
                    }}
                />
            }
            {
                user &&
                <Row type="flex" justify="center">
                    <Col style={{ padding: "30px 0px 50px 0px" }}>
                        <div style={{
                            background: "#FFE4B5",
                            padding: "20px 30px",
                            fontSize: 24,
                            color: "#000000",
                            borderRadius: "10px"
                        }}>
                            Welcome! {user.username}
                        </div>
                    </Col>
                </Row>
            }
        </div>
    );
}

export default UserInfo;
