import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { fetchUser } from '../../reducers/LoginReducer';
import { fetchQuiz } from '../../reducers/SetQuizReducer';
import 'antd/dist/antd.css';
import '../../css/login.css';
import LoginForm from './LoginForm';
import { Card, Typography, Row, Col } from 'antd';
import Icon from '../../image/iconLogin.png';

const { Title } = Typography;

const Login = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchUser())
        dispatch(fetchQuiz())
    }, [])

    return (
        <div className='loginPage-container'>
            <Row type="flex" justify="center" align="middle" style={{ minHeight: "100vh" }}>
                <Col>
                    <Card style={{ maxWidth: "500px", minWidth: "200px" }}>
                        <Title style={{ textAlign: "center", marginTop: 30 }}>Login</Title>
                        <img className="userIcon" src={Icon} alt="userIcon" />
                        <LoginForm />
                    </Card>
                </Col>
            </Row>
        </div>
    );
}

export default Login;
