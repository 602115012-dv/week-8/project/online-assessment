import React from 'react';
import { useSelector } from 'react-redux';
import { Form, Input, Button, Row, Col } from 'antd';
import { getUserName, getPassword } from '../../reducers/LoginReducer';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';

const LoginForm = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const users = useSelector(state => state.checklogin);

    return (
        <Form className="login-form" >
            <Row type="flex" justify="center">
                <Col span={4}>
                    <Form.Item
                        label={<span style={{ color: "black" }}>Username :</span>}
                        className="loginLabel" />
                </Col>
                <Col span={15}>
                    <Input
                        placeholder="Please input your username..."
                        name="username"
                        value={users.username}
                        onChange={(e) => dispatch(getUserName(e.target.value))}
                        className="inputUsername"
                    />
                </Col>
            </Row>
            <Row type="flex" justify="center">
                <Col span={4}>
                    <Form.Item
                        label={<span style={{ color: "black" }}>Password :</span>}
                        className="loginLabel"></Form.Item>
                </Col>
                <Col span={15}>
                    <Input
                        type="password"
                        placeholder="Please input your password..."
                        name="password"
                        value={users.password}
                        onChange={(e) => dispatch(getPassword(e.target.value))}
                        className="inputUsername"
                    />
                </Col>
            </Row>
            <Row type="flex" justify="center">
                <Col>
                    <Button
                        type="primary"
                        htmlType="submit"
                        onClick={() => 
                            { history.push(`/processLogin/${users.username}/${users.password}`) }}
                        style={{
                            padding: "10px 20px",
                            height: "auto",
                            width: "auto",
                            margin: "20px 0px"
                        }}
                        className="loginBtn"
                    >
                        Login
                    </Button>
                </Col>
            </Row>
        </Form >
    );
}

export default LoginForm;
