import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

const PrivateRoute = ({ path, component: Component, ...props }) => {
const {isLogin} = useSelector(state => state.checklogin)
     
return(
        <Route path={path} render={(props) => {

            if (isLogin === true) {
                return <Component {...props} />
            }
            else {
                return <Redirect to="/login" />
            }
        }} {...props}/>
    )
}

export default PrivateRoute;
