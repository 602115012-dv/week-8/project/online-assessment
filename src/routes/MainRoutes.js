import React, { useEffect } from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import LoginRedux from '../component/Login/LoginRedux';
import { login, logout } from '../actions/loginAction';
import HomeRedux from '../component/Home/HomeRedux';
import PrivateRoute from './PrivateRoutes';
import UserQuizRedux from '../components/UserQuizPage/UserQuizRedux';
import CreateQuiz from '../components/CreateQuiz/CreateQuiz';
import EditQuiz from '../components/EditQuiz/EditQuiz';
import DoingQuizPage from '../components/doingQuiz/doingQuizPage';
import HistoryPage from '../components/HistoryPage/HistoryPage';
import ResultPage from '../components/result.js/ResultPage';
import AnswerPage from '../components/Answer/Answer';
import { fetchUser } from '../reducers/SetUserReducer';

const MainRoutes = () => {


  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUser())
  }, [])

  return (
    <BrowserRouter>
      <Route exact path="/" render={() => {
        return <Redirect to="/login" />
      }} />
      <Route path="/processLogin/:username/:password" render={(props) => {
        let username = props.match.params.username;
        let password = props.match.params.password;
        dispatch(login(username, password))
        return <Redirect to="/home" />
      }}></Route>

      <Route path="/processLogout" render={() => {
        dispatch(logout())
        return <Redirect to="/" />
      }}></Route>

      <Route exact path="/login" component={LoginRedux} />
      <PrivateRoute exact path='/home' component={HomeRedux} />
      <PrivateRoute path="/userquiz" component={UserQuizRedux} />
      <PrivateRoute path="/quiz/create" component={CreateQuiz} />
      <PrivateRoute path="/quiz/edit/:id" component={EditQuiz} />
      <PrivateRoute exact path="/doingQuiz/:quiz_id" component={DoingQuizPage} />
      <PrivateRoute path="/history" component={HistoryPage} exact={true} />
      <PrivateRoute path="/result/:quiz_id" component={ResultPage} exact={true} />
      <PrivateRoute path="/answer/:quiz_id" component={AnswerPage} exact={true} />
    </BrowserRouter>
  );
}
export default MainRoutes;
