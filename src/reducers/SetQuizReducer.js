import { environment } from "../constants/environment";

const FETCH_QUIZ_BEGIN = 'FETCH_QUIZ_BEGIN';
const FETCH_QUIZ_SUCCESS = 'FETCH_QUIZ_SUCCESS';
const FETCH_QUIZ_ERROR = 'FETCH_QUIZ_ERROR';


export const fetchQuiz = () => {
    return dispatch => {
        dispatch(fetchQuizBegin())
        return fetch(environment.quizzesApi)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchQuizSuccess(data))
                //alert('yoyo')
            })
            .catch(error => dispatch(fetchQuizError(error)))

    }
}

export const fetchQuizBegin = () => {
    return {
        type: FETCH_QUIZ_BEGIN
    }
}
export const fetchQuizSuccess = (quiz) => {
    return {
        type: FETCH_QUIZ_SUCCESS,
        payLoad: quiz
    }
}
export const fetchQuizError = (error) => {
    return {
        type: FETCH_QUIZ_ERROR,
        payLoad: error
    }
}

const QuizState = {

    quiz: [],
    loading: false,
    error: ''

}

export const setQuizReducer = (state = QuizState, action) => {
    switch (action.type) {

        case FETCH_QUIZ_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_QUIZ_SUCCESS:
            return {
                quiz: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_QUIZ_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        default:
            return state
    }
}

