import { fetchUserBegin, fetchUserSuccess, fetchUserError } from './SetUserReducer';
import { environment } from '../constants/environment';

const LOGIN = 'LOGIN';
const LOGOUT = 'LOGOUT';
const USERNAME = 'USERNAME';
const PASSWORD = 'PASSWORD';
const SETUSER = 'SETUSER';

export const fetchUser = () => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch(environment.usersApi)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
                dispatch(setUser(data))
            })
            .catch(error => dispatch(fetchUserError(error)))
    }
}

export const getUserName = (username) => {
    return {
        type: USERNAME,
        userName: username
    }
}

export const getPassword = (password) => {
    return {
        type: PASSWORD,
        passWord: password
    }
}

export const login = (username, password) => {
    localStorage.setItem("isLogin", true)
    return {
        type: LOGIN,
        payload: {
            username: username,
            password: password
        }
    }
}

export const logout = () => {
    localStorage.setItem("isLogin", false)
    return {
        type: LOGOUT
    }
}

export const setUser = (users) => {
    return {
        type: SETUSER,
        payload: users
    }
}

const initialState = {
    username: '',
    password: '',
    currentUserId: '',
    isLogin: false,
    users: []
}

export const LoginReducer = (state = initialState, action) => {
    switch (action.type) {
        case SETUSER:
            //alert('yuuyuyuy')
            return {
                ...state,
                users: action.payload
            }
        case LOGIN:
            //console.log(state.users)
            //console.log(action.payload.username)
            let user = state.users.find((user) => user.username == action.payload.username && user.password == action.payload.password)
            //alert(user)
            // alert(JSON.stringify(user));

            if (user) {
                //alert(user[0].id)
                return {
                    ...state,
                    isLogin: true,
                    currentUserId: user.id,
                    user: user
                }
            }
            else {
                return {
                    ...state,
                    isLogin: false,
                    username: '',
                    password: ''

                }

            }
        case LOGOUT:
return {
    ...state,
    isLogin: false
}
        case USERNAME:
return {
    ...state,
    username: action.userName
}
        case PASSWORD:
return {
    ...state,
    password: action.passWord
}

        default:
return state
    }
}
