import { 
  FETCH_QUIZ_BY_ID_BEGIN, 
  FETCH_QUIZ_BY_ID_SUCCEED, 
  FETCH_QUIZ_BY_ID_FAIL, 
  SET_QUIZ_EDITING_SUBMIT_FUNCTION, 
  SET_QUIZ_EDITING_IMAGE, 
  SET_QUIZ_EDITING_IMAGE_FILE, 
  UPDATE_EDITING_QUESTION, 
  UPDATE_EDITING_QUESTION_IMAGE_FILE, 
  UPDATE_EDITING_QUESTION_IMAGE_URL, 
  ADD_QUESTION, 
  DELETE_QUESTION } from "../constants/actionTypes";

const initialState = {
  quiz: null,
  loading: false,
  error: null,
  submitFormFunc: null,
  quizImageFile: null,
  questionImageFiles: []
}

export const editQuizReducer = (state = initialState, action) => {
  let quiz = Object.assign({}, state.quiz);
  switch (action.type) {
    case FETCH_QUIZ_BY_ID_BEGIN:
      return {
        ...state,
        loading: true
      }
    case FETCH_QUIZ_BY_ID_SUCCEED:
      return {
        ...state,
        loading: false,
        quiz: action.payload
      }
    case FETCH_QUIZ_BY_ID_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload
      }
    case SET_QUIZ_EDITING_SUBMIT_FUNCTION:
      return {
        ...state,
        submitFormFunc: action.payload
      }
    case SET_QUIZ_EDITING_IMAGE:
      quiz.image = action.payload;
      return {
        ...state,
        quiz
      }
    case SET_QUIZ_EDITING_IMAGE_FILE:
      return {
        ...state,
        quizImageFile: action.payload
      }
    case UPDATE_EDITING_QUESTION:
      if (action.payload.updateOp.propName === 'answers.value') {
        quiz.questions[action.payload.index].answers[action.payload.updateOp.answerIndex].value = action.payload.updateOp.value;
      } else if (action.payload.updateOp.propName === "answers.isCorrectAnswer") {
        quiz.questions[action.payload.index]["answers"] = quiz.questions[action.payload.index]["answers"].map((answer, index) => {
          if (action.payload.updateOp.answerIndex !== index) {
            answer.isCorrectAnswer = false;
            return answer
          } else {
            answer.isCorrectAnswer = true;
            return answer
          }
        })
        console.log(quiz.questions)
      } else {
        quiz.questions[action.payload.index][action.payload.updateOp.propName] = action.payload.updateOp.value
      }

      return {
        ...state,
        quiz
      }
    case UPDATE_EDITING_QUESTION_IMAGE_FILE:
      let questionImageFiles = [...state.questionImageFiles];
      questionImageFiles[action.payload.index] = action.payload.file;
      return {
        ...state,
        questionImageFiles
      }
    case UPDATE_EDITING_QUESTION_IMAGE_URL:
      quiz.questions[action.payload.index].image = action.payload.imageUrl;
      return {
        ...state,
        quiz
      }
    case ADD_QUESTION:
      quiz.questions.push({
        title: null,
        image: null,
        score: null,
        answers: [
          {
            value: null,
            isCorrectAnswer: false
          },
          {
            value: null,
            isCorrectAnswer: false
          },
          {
            value: null,
            isCorrectAnswer: false
          },
          {
            value: null,
            isCorrectAnswer: false
          }
        ]
      })
      return {
        ...state,
        quiz
      }
    case DELETE_QUESTION:
      quiz.questions.splice(action.payload, 1);
      return {
        ...state,
        quiz
      }
    default:
      return state;
  }
}