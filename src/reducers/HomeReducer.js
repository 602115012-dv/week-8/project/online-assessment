const FILTER_TYPE = 'FILTER_TYPE';
const SEARCH_QUIZ = 'SEARCH_QUIZ';

export const filterType = (value) => {
    return {
        type: FILTER_TYPE,
        payLoad: value
    }
}

export const searchText = (text) => {
    return {
        type: SEARCH_QUIZ,
        payLoad: text
    }
}

const initialState = {

    filter: -1,
    searchText: ''

}


export const homeReducer = (state = initialState, action) => {
    switch (action.type) {
       
        case FILTER_TYPE:
            return {
                ...state,
                filter: action.payLoad
            }
        case SEARCH_QUIZ:
            return {
                ...state,
                searchText: action.payLoad
            }
        default:
            return state
    }
}

