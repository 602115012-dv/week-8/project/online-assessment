import { combineReducers } from 'redux';
import { setUserReducer } from './SetUserReducer';
import { setQuizReducer } from './SetQuizReducer';
import { homeReducer } from './HomeReducer';
import { LoginReducer } from './LoginReducer';
import { userQuizReducer } from '../reductions/UserQuizReducer';
import { searchReducer } from '../reductions/SearchReducer';
import { DoingQuizPageReducer } from './doingQuizPageReducer';
import { HistoryReducer } from './history/HistoryReducer';
import { ResultReducer } from './Result/ResultReducer';
import { createQuizReducer } from './createQuizReducer';
import { editQuizReducer } from './editQuizReducer';
import { AnswerReducer } from './AnswerReducer';

const rootReducer = combineReducers({
  users: setUserReducer,
  quizs: setQuizReducer,
  home: homeReducer,
  checklogin: LoginReducer,
  userQuizList: userQuizReducer,
  search: searchReducer,
  createQuizReducer,
  editQuizReducer,
  doingQuizPage: DoingQuizPageReducer,
  historyCompState: HistoryReducer,
  resultCompState: ResultReducer,
  answerCompState: AnswerReducer
});
export default rootReducer;
