import {
  FETCH_USER_RESULT_BEGIN,
  FETCH_USER_RESULT_SUCCESS,
  FETCH_USER_RESULT_ERROR
} from "../../constants/actionTypes";

export const initialHistory = {
  userResult: [],
  loading: false,
  error: '',
  quizFullScore: [],
}


export const ResultReducer = (state = initialHistory, action) => {
  switch (action.type) {

    case FETCH_USER_RESULT_BEGIN:
      return {
        ...state,
        loading: true
      }
    case FETCH_USER_RESULT_SUCCESS:
      const place = action.payLoad.userScoreList.sort((a, b) => {
        return b.score - a.score;
      })
      return {
        ...state,
        userResult: place,
        loading: false,
        error: '',
        quizFullScore: action.payLoad
      }
    case FETCH_USER_RESULT_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payLoad
      }
    default:
      return state
  }
}
