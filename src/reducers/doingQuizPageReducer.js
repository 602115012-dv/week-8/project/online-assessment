import { FETCH_QUIZ_DETAIL_BEGIN, FETCH_QUIZ_DETAIL_SUCCESS, NEXT_QUESTION, PREVIOUS_QUESTION, SELECT_ANSWER, SUBMIT_ANSWER, FETCH_QUIZ_DETAIL_ERROR, RESET_DOING_STATE, SET_IS_UPDATE_USER, SET_IS_UPDATE_QUIZ, SET_SUBMIT_CLICKED } from "../constants/actionTypes";

const initialState = {
    quizDetail: {},
    questions: [],
    loadingQuizDetail: false,
    errorQuizDetail: '',
    currentQuestion: 0,
    answers: [],
    score: 0,
    isUpdatingUser: false,
    isUpdatingQuiz: false,
    submitClicked: false
}

export const DoingQuizPageReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_QUIZ_DETAIL_BEGIN:
            return {
                ...state,
                loadingQuizDetail: true
            };
        case FETCH_QUIZ_DETAIL_SUCCESS:
            return {
                ...state,
                loadingQuizDetail: false,
                quizDetail: action.payLoad,
                questions: action.payLoad.questions
            };
        case FETCH_QUIZ_DETAIL_ERROR:
            return {
                ...state,
                loadingQuizDetail: false,
                errorQuizDetail: action.payLoad
            };
        case NEXT_QUESTION:
            return {
                ...state,
                currentQuestion: state.currentQuestion + 1
            };
        case PREVIOUS_QUESTION:
            return {
                ...state,
                currentQuestion: state.currentQuestion - 1
            };
        case SELECT_ANSWER:
            var temp = [...state.answers];
            if (temp[state.currentQuestion] == undefined) {
                temp.push({
                    value: action.payLoad,
                    choice: state.questions[state.currentQuestion].answers[action.payLoad],
                    score: state.questions[state.currentQuestion].score
                });
            } else {
                temp[state.currentQuestion].choice = state.questions[state.currentQuestion].answers[action.payLoad];
                temp[state.currentQuestion].value = action.payLoad;
                temp[state.currentQuestion].score = state.questions[state.currentQuestion].score;
            }
            return {
                ...state,
                answers: temp
            }
        case SET_IS_UPDATE_USER:
            return {
                ...state,
                isUpdatingUser: action.payLoad
            }
        case SET_IS_UPDATE_QUIZ:
            return {
                ...state,
                isUpdatingQuiz: action.payLoad
            }
        case SET_SUBMIT_CLICKED:
            return {
                ...state,
                submitClicked: action.payLoad
            }
        case RESET_DOING_STATE:
            return initialState;
        default:
            return state;
    }
}
