import {
  FETCH_USER_HISTORY_BEGIN,
  FETCH_USER_HISTORY_SUCCESS,
  FETCH_USER_HISTORY_ERROR
} from "../../constants/actionTypes";
import {
  FETCH_QUIZ_HISTORY_BEGIN,
  FETCH_QUIZ_HISTORY_SUCCESS,
  FETCH_QUIZ_HISTORY_ERROR
} from '../../constants/actionTypes';


export const initialHistory = {
  userHistory: [],
  quizzes: [],
  loading: false,
  error: ''
}

export const HistoryReducer = (state = initialHistory, action) => {
  switch (action.type) {

    case FETCH_USER_HISTORY_BEGIN:
      return {
        ...state,
        loading: true
      }
    case FETCH_USER_HISTORY_SUCCESS:
      return {
        ...state,
        userHistory: action.payLoad.history,
        loading: false,
        error: ''
      }
    case FETCH_USER_HISTORY_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payLoad
      }
    case FETCH_QUIZ_HISTORY_BEGIN:
      return {
        ...state,
        loading: true
      }
    case FETCH_QUIZ_HISTORY_SUCCESS:
      // const quizIdFromUser = state.userHistory;
      // const quizId = action.payLoad;
      // const temp = [];
      // console.log(quizId._id)
      // for (var i = 0; i < quizId.length; i++) {
      //   if (quizId._id == quizIdFromUser.quiz) {
      //     temp.push(quizId[i]);
      //   }
      // }

      return {
        ...state,
        quizzes: action.payLoad,
        loading: false,
        error: ''
      }
    case FETCH_QUIZ_HISTORY_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payLoad
      }
    default:
      return state
  }
}