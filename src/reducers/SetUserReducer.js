import { setUser } from "./LoginReducer";
import { environment } from "../constants/environment";
const FETCH_USER_BEGIN = 'FETCH_USER_BEGIN';
const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
const FETCH_USER_ERROR = 'FETCH_USER_ERROR';


export  const fetchUser = () => {
    return dispatch => {
        //alert('ddddd')
        dispatch(fetchUserBegin())
        return fetch(environment.usersApi)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
                dispatch(setUser(data))
                // console.log(data)
            })
            .catch(error => dispatch(fetchUserError(error)))
           
    }
}

export const fetchUserBegin = () => {
    return {
        type: FETCH_USER_BEGIN
    }
}
export const fetchUserSuccess = (users) => {
    return {
        type: FETCH_USER_SUCCESS,
        payLoad: users
    }
}
export const fetchUserError = (error) => {
    return {
        type: FETCH_USER_ERROR,
        payLoad: error
    }
}

const UserState = {

    users: [],
    loading: false,
    error: ''

}

export const setUserReducer = (state = UserState, action) => {
    switch (action.type) {

        case FETCH_USER_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_USER_SUCCESS:
            console.log(action.payLoad)
            return {
                users: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_USER_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        default:
            return state
    }
}

