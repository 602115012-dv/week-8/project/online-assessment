import {
  FETCH_QUIZ_ANSWER_BEGIN,
  FETCH_QUIZ_ANSWER_SUCCESS,
  FETCH_QUIZ_ANSWER_ERROR
} from '../constants/actionTypes';

export const initialAnswer = {
  loading: false,
  error: '',
  quizQuestion: [],
  quiz: [],
}

export const AnswerReducer = (state = initialAnswer, action) => {
  switch (action.type) {

    case FETCH_QUIZ_ANSWER_BEGIN:
      return {
        ...state,
        loading: true
      }
    case FETCH_QUIZ_ANSWER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: '',
        quizQuestion: action.payLoad.questions,
        quiz: action.payLoad
      }
    case FETCH_QUIZ_ANSWER_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payLoad
      }
    default:
      return state
  }
}
