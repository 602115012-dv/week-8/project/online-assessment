import { 
  SET_QUIZ_IMAGE_FILE, 
  SET_QUIZ_IMAGE_URL,
  BEGIN_IMAGE_UPLOADING,
  END_IMAGE_UPLOADING,
  UPDATE_QUESTION,
  UPDATE_QUESTION_IMAGE_FILE,
  UPDATE_QUESTION_IMAGE_URL,
  ADD_FORM,
  SET_QUIZ_SUBMIT_FUNCTION,
  DELETE_CREATING_QUESTION,
  ADD_CREATING_QUESTION
} from "../constants/actionTypes"

const initialState = {
  quizImage: null,
  quizImageFile: null,
  answersImageFiles: [],
  isImageUploading: false,
  questionSize: 1,
  forms: [],
  submitFunc: null,
  questions: [
    {
      title: null,
      image: null,
      score: null,
      answers: [
        {
          value: null,
          isCorrectAnswer: false
        },
        {
          value: null,
          isCorrectAnswer: false
        },
        {
          value: null,
          isCorrectAnswer: false
        },
        {
          value: null,
          isCorrectAnswer: false
        }
      ]
    }
  ]
}

export const createQuizReducer = (state = initialState, action) => {
  let questions = [...state.questions];
  switch (action.type) {
    case SET_QUIZ_IMAGE_FILE:
      return {
        ...state,
        "quizImageFile": action.payload
      };
    case SET_QUIZ_IMAGE_URL:
      console.log('set quiz iamge file');
      return {
        ...state,
        quizImage: action.payload
      }
    case BEGIN_IMAGE_UPLOADING:
      return {
        ...state,
        isImageUploading: true
      }
    case END_IMAGE_UPLOADING:
      return {
        ...state,
        isImageUploading: false
      }
    case ADD_CREATING_QUESTION:
      questions = [...state.questions];
      questions.push(
        {
          title: null,
          image: null,
          score: null,
          answers: [
            {
              value: null,
              isCorrectAnswer: false
            },
            {
              value: null,
              isCorrectAnswer: false
            },
            {
              value: null,
              isCorrectAnswer: false
            },
            {
              value: null,
              isCorrectAnswer: false
            }
          ]
        }
      ) 
      return {
        ...state,
        questionSize: state.questionSize + 1,
        questions: questions
      }
    case UPDATE_QUESTION:
      questions = [...state.questions];
      if (action.payload.updateOps.propName === "answers.value") {
        questions[action.payload.index]["answers"][action.payload.updateOps.answerIndex].value = 
            action.payload.updateOps.value;
      } else if (action.payload.updateOps.propName === "answers.isCorrectAnswer") {
        questions[action.payload.index]["answers"] = questions[action.payload.index]["answers"].map((answer, index) => {
          if (action.payload.updateOps.answerIndex !== index) {
            answer.isCorrectAnswer = false;
            return answer
          } else {
            answer.isCorrectAnswer = true;
            return answer
          }
        })
        console.log(questions)
      } else {
        questions[action.payload.index][action.payload.updateOps.propName] = 
        action.payload.updateOps.value;
      }
      return {
        ...state,
        questions: questions
      }
    case UPDATE_QUESTION_IMAGE_FILE:
      let answersImageFiles = [...state.answersImageFiles];
      answersImageFiles[action.payload.index] = action.payload.file;
      return {
        ...state,
        answersImageFiles: answersImageFiles
      }
    case UPDATE_QUESTION_IMAGE_URL:
      questions[action.payload.index].image = action.payload.imageUrl;
      return {
        ...state,
        questions: questions
      }
    case ADD_FORM:
      console.log(action)
      let forms = [...state.forms];
      forms.push(action.payload);
      return {
        ...state,
        forms: forms
      }
    case SET_QUIZ_SUBMIT_FUNCTION:
      return {
        ...state,
        submitFunc: action.payload
      }
    case DELETE_CREATING_QUESTION:
      console.log('pass')
      questions.splice(action.payload, 1);
      console.log(questions)
      return {
        ...state,
        questions,
        questionSize: state.questionSize - 1
      }
    default:
      return state;
  }
}