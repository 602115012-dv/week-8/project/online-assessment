import {
  FETCH_QUIZ_ANSWER_BEGIN,
  FETCH_QUIZ_ANSWER_SUCCESS,
  FETCH_QUIZ_ANSWER_ERROR
} from '../constants/actionTypes';
import { environment } from '../constants/environment';

//insert UserId vvv
export const fetchQuizzes = (quizId) => {
  return dispatch => {
    dispatch(fetchQuizAnswerBegin())
    return fetch(environment.quizzesApi + quizId)
      .then(res => res.json())
      .then(data => {
        dispatch(fetchQuizAnswerSuccess(data))
      })
      .catch(error => dispatch(fetchQuizAnswerError(error)))
  }
}

export const fetchQuizAnswerBegin = () => {
  return {
    type: FETCH_QUIZ_ANSWER_BEGIN
  }
}

export const fetchQuizAnswerSuccess = quiz => {
  return {
    type: FETCH_QUIZ_ANSWER_SUCCESS,
    payLoad: quiz
  }
}

export const fetchQuizAnswerError = error => {
  return {
    type:   FETCH_QUIZ_ANSWER_ERROR,
    payLoad: error
  }
}
