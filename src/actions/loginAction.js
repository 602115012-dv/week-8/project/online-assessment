import { fetchUserBegin, fetchUserSuccess, fetchUserError } from '../reducers/SetUserReducer';
import { USERNAME,PASSWORD,LOGIN,LOGOUT,SETUSER } from '../constants/actionTypes';
import { environment } from '../constants/environment';

export const fetchUser = () => {
    return dispatch => {
        dispatch(fetchUserBegin())
        return fetch(environment.usersApi)
            .then(res => res.json())
            .then(data => {
                dispatch(fetchUserSuccess(data))
                dispatch(setUser(data))
            })
            .catch(error => dispatch(fetchUserError(error)))
    }
}

export const getUserName = (username) => {
    return {
        type: USERNAME,
        userName: username
    }
}

export const getPassword = (password) => {
    return {
        type: PASSWORD,
        passWord: password
    }
}

export const login = (username, password) => {
    localStorage.setItem("isLogin", true)
    return {
        type: LOGIN,
        payload: {
            username: username,
            password: password
        }
    }
}

export const logout = () => {
    localStorage.setItem("isLogin", false)
    return {
        type: LOGOUT
    }
}

export const setUser = (users) => {
    return {
        type: SETUSER,
        payload: users
    }
}