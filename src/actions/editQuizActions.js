import { FETCH_QUIZ_BY_ID_BEGIN, FETCH_QUIZ_BY_ID_SUCCEED, FETCH_QUIZ_BY_ID_FAIL, SET_QUIZ_EDITING_SUBMIT_FUNCTION, SET_QUIZ_EDITING_IMAGE, SET_QUIZ_EDITING_IMAGE_FILE, UPDATE_EDITING_QUESTION, UPDATE_EDITING_QUESTION_IMAGE_FILE, UPDATE_EDITING_QUESTION_IMAGE_URL, DELETE_QUESTION, ADD_QUESTION } from "../constants/actionTypes";
import axios from 'axios';
import { environment } from '../constants/environment';

export const fetchQuizByIdBegin = () => (
  {
    type: FETCH_QUIZ_BY_ID_BEGIN
  }
);

export const fetchQuizByIdSucceed = (quiz) => (
  {
    type: FETCH_QUIZ_BY_ID_SUCCEED,
    payload: quiz
  }
);

export const fetchQuizByIdFail = (err) => (
  {
    type: FETCH_QUIZ_BY_ID_FAIL,
    payload: err
  }
);

export const fetchQuizById = (quizId) => {
  return dispatch => {
    dispatch(fetchQuizByIdBegin());
    return axios.get(`${environment.quizzesApi}/${quizId}`)
        .then(res => {
          dispatch(fetchQuizByIdSucceed(res.data))
        })
        .catch(err => dispatch(fetchQuizByIdFail(err)));
  }
}

export const setQuizEditingSubmitFunction = (submitFunc) => (
  {
    type: SET_QUIZ_EDITING_SUBMIT_FUNCTION,
    payload: submitFunc
  }
);

export const setQuizEditingImage = (imageUrl) => (
  {
    type: SET_QUIZ_EDITING_IMAGE,
    payload: imageUrl
  }
);

export const setQuizEditingImageFile = (file) => (
  {
    type: SET_QUIZ_EDITING_IMAGE_FILE,
    payload: file
  }
);

export const updateEditingQuestion = (index, updateOp) => (
  {
    type: UPDATE_EDITING_QUESTION,
    payload: {
      index,
      updateOp
    }
  }
);

export const updateEditingQuestionImageFile = (index, file) => (
  {
    type: UPDATE_EDITING_QUESTION_IMAGE_FILE,
    payload: {
      index,
      file
    }
  }
);

export const updateEditingQuestionImageUrl = (index, imageUrl) => (
  {
    type: UPDATE_EDITING_QUESTION_IMAGE_URL,
    payload: {
      index,
      imageUrl
    }
  }
);

export const deleteQuestion = (index) => (
  {
    type: DELETE_QUESTION,
    payload: index
  }
);

export const addQuestion = () => (
  {
    type: ADD_QUESTION
  }
)