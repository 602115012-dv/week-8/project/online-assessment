import { FETCH_QUIZ_DETAIL_BEGIN, FETCH_QUIZ_DETAIL_SUCCESS, FETCH_QUIZ_DETAIL_ERROR, NEXT_QUESTION, PREVIOUS_QUESTION, SELECT_ANSWER, SUBMIT_ANSWER, RESET_DOING_STATE, SET_IS_UPDATE_USER, SET_IS_UPDATE_QUIZ, SET_SUBMIT_CLICKED } from "../constants/actionTypes";
import { environment } from "../constants/environment";

// Fetch quiz
export const fetchQuizDetail = (quizId) => {
    return dispatch => {
        dispatch(fetchQuizDetailBegin(quizId));
        fetch(environment.quizzesApi + quizId)
            .then(response => response.json())
            .then(data => {
                dispatch(fetchQuizDetailSuccess(data));
            })
            .catch(error => {
                dispatch(fetchQuizDetailError(error));
            });
    }
};

export const fetchQuizDetailBegin = () => {
    return {
        type: FETCH_QUIZ_DETAIL_BEGIN
    };
};

export const fetchQuizDetailSuccess = (quiz) => {
    return {
        type: FETCH_QUIZ_DETAIL_SUCCESS,
        payLoad: quiz
    };
};

export const fetchQuizDetailError = (error) => {
    return {
        type: FETCH_QUIZ_DETAIL_ERROR,
        payLoad: error
    };
};

// choose question
export const nextQuestion = () => {
    return {
        type: NEXT_QUESTION
    };
};

export const previousQuestion = () => {
    return {
        type: PREVIOUS_QUESTION
    };
};

export const selectAnswer = (answer) => {
    return {
        type: SELECT_ANSWER,
        payLoad: answer
    };
};

export const resetDoingState = () => {
    return {
        type: RESET_DOING_STATE
    };
};

export const setIsUpdateUser = (status) => {
    return {
        type: SET_IS_UPDATE_USER,
        payLoad: status
    };
};

export const setIsUpdateQuiz = (status) => {
    return {
        type: SET_IS_UPDATE_QUIZ,
        payLoad: status
    };
};

export const setSubmitClicked = (status) => {
    return {
        type: SET_SUBMIT_CLICKED,
        payLoad: status
    };
};
