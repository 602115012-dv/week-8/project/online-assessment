
import {
    FETCH_USER_QUIZ_BEGIN, FETCH_USER_QUIZ_SUCCESS, FETCH_USER_QUIZ_ERROR
    , SEARCH_DATA, DELETE_BEGIN, DELETE_SUCCESS, DELETE_ERROR
} from '../constants/actionTypes';
import Axios from 'axios';
import { environment } from '../constants/environment';


export const fetchUserQuiz = userId => {
    return dispatch => {
        dispatch(fetchUserQuizBegin())
        fetch(environment.quizzesApi)
            .then(res => res.json())
            .then(data => {
                var temp = data.filter(item => {
                    console.log('item', item)
                    return item.user._id == userId
                });
                dispatch(fetchUserQuizSuccess(temp))
            })
            .catch(error => dispatch(fetchUserQuizError(error)))
    }
}
export const fetchUserQuizBegin = () => {
    return {
        type: FETCH_USER_QUIZ_BEGIN,
    }
}

export const fetchUserQuizSuccess = (data) => {
    return {
        type: FETCH_USER_QUIZ_SUCCESS,
        payLoad: data
    }
}

export const fetchUserQuizError = error => {
    return {
        type: FETCH_USER_QUIZ_ERROR,
        payLoad: error
    }
}

export const searchData = (value) => {
    return {
        type: SEARCH_DATA,
        payLoad: value
    }
}

export const deleteUserQuiz = (quizId, index) => {
    return dispatch => {
        dispatch(deleteBegin())
        const axios = require('axios');
        axios.delete(environment.quizzesApi + quizId)
            .then(data => {
                console.log(data)
                dispatch(deleteSuccess(index))
            })
            .catch(error => dispatch(deleteError(error)))
    }
}
export const deleteBegin = () => {
    return {
        type: DELETE_BEGIN,
    }
}

export const deleteSuccess = index => {
    return {
        type: DELETE_SUCCESS,
        payLoad: index
    }
}

export const deleteError = error => {
    return {
        type: DELETE_ERROR,
        payLoad: error
    }
}

