import {FILTER_TYPE,SEARCH_QUIZ} from '../constants/actionTypes';

export const filterType = (value) => {
    return {
        type: FILTER_TYPE,
        payLoad: value
    }
}

export const searchText = (text) => {
    return {
        type: SEARCH_QUIZ,
        payLoad: text
    }
}