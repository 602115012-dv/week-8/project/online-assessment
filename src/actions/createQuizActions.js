import { 
  SET_QUIZ_IMAGE_FILE, 
  SET_QUIZ_IMAGE_URL,
  BEGIN_IMAGE_UPLOADING,
  END_IMAGE_UPLOADING,
  ADD_QUESTION,
  UPDATE_QUESTION,
  UPDATE_QUESTION_IMAGE_FILE,
  UPDATE_QUESTION_IMAGE_URL,
  ADD_FORM,
  SET_QUIZ_SUBMIT_FUNCTION,
  DELETE_CREATING_QUESTION,
  ADD_CREATING_QUESTION
} from '../constants/actionTypes';


export const setQuizImageFile = (file) => (
  {
    type: SET_QUIZ_IMAGE_FILE,
    payload: file
  }
);

export const setQuizImageUrl = (imageUrl) => (
  {
    type: SET_QUIZ_IMAGE_URL,
    payload: imageUrl
  }
);

export const beginImageUploading = () => (
  {
    type: BEGIN_IMAGE_UPLOADING
  }
);

export const endImageUploading = () => (
  {
    type: END_IMAGE_UPLOADING
  }
);

export const addQuestion = () => (
  {
    type: ADD_CREATING_QUESTION
  }
);

export const updateQuestion = (index, updateOps) => (
  {
    type: UPDATE_QUESTION,
    payload: {
      index,
      updateOps
    }
  }
)

export const updateQuestionImageFile = (index, file) => (
  {
    type: UPDATE_QUESTION_IMAGE_FILE,
    payload: {
      index,
      file
    }
  }
);

export const updateQuestionImageUrl = (index, imageUrl) => (
  {
    type:UPDATE_QUESTION_IMAGE_URL,
    payload: {
      index,
      imageUrl
    }
  }
);

export const addForm = (form) => (
  {
    type: ADD_FORM,
    payload: form
  }
)

export const setQuizSubmitFunction = (submitFunc) => (
  {
    type: SET_QUIZ_SUBMIT_FUNCTION,
    payload: submitFunc
  }
);

export const deleteQuestion = (index) => (
  {
    type: DELETE_CREATING_QUESTION,
    payload: index
  }
);