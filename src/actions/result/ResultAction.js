import {
  FETCH_USER_RESULT_BEGIN,
  FETCH_USER_RESULT_SUCCESS,
  FETCH_USER_RESULT_ERROR
} from '../../constants/actionTypes';
import { environment } from '../../constants/environment';

//insert UserId vvv
export const fetchQuizzes = (quizId) => {
  return dispatch => {
    dispatch(fetchUserHistoryBegin())
    return fetch(environment.quizzesApi + quizId)
      .then(res => res.json())
      .then(data => {
        dispatch(fetchUserHistorySuccess(data))
      })
      .catch(error => dispatch(fetchUserHistoryError(error)))
  }
}

export const fetchUserHistoryBegin = () => {
  return {
    type: FETCH_USER_RESULT_BEGIN
  }
}

export const fetchUserHistorySuccess = todos => {
  return {
    type: FETCH_USER_RESULT_SUCCESS,
    payLoad: todos
  }
}

export const fetchUserHistoryError = error => {
  return {
    type:   FETCH_USER_RESULT_ERROR,
    payLoad: error
  }
}
