import {
  FETCH_USER_HISTORY_BEGIN,
  FETCH_USER_HISTORY_SUCCESS,
  FETCH_USER_HISTORY_ERROR
} from '../../constants/actionTypes';

import {
  FETCH_QUIZ_HISTORY_BEGIN,
  FETCH_QUIZ_HISTORY_SUCCESS,
  FETCH_QUIZ_HISTORY_ERROR
} from '../../constants/actionTypes';
import { environment } from '../../constants/environment';

//insert UserId vvv
export const fetchUserHistory = (userId) => {
  return dispatch => {
    dispatch(fetchUserHistoryBegin())
    return fetch(environment.usersApi + userId)
      .then(res => res.json())
      .then(data => {
        dispatch(fetchUserHistorySuccess(data))
      })
      .catch(error => dispatch(fetchUserHistoryError(error)))
  }
}

export const fetchUserHistoryBegin = () => {
  return {
    type: FETCH_USER_HISTORY_BEGIN
  }
}

export const fetchUserHistorySuccess = todos => {
  return {
    type: FETCH_USER_HISTORY_SUCCESS,
    payLoad: todos
  }
}

export const fetchUserHistoryError = error => {
  return {
    type: FETCH_USER_HISTORY_ERROR,
    payLoad: error
  }
}


//insert UserId vvv
export const fetchQuizzes = () => {
  return dispatch => {
    dispatch(fetchQuizHistoryBegin())
    return fetch(environment.quizzesApi)
      .then(res => res.json())
      .then(data => {
        dispatch(fetchQuizHistorySuccess(data))
      })
      .catch(error => dispatch(fetchQuizHistoryError(error)))
  }
}

export const fetchQuizHistoryBegin = () => {
  return {
    type: FETCH_QUIZ_HISTORY_BEGIN
  }
}

export const fetchQuizHistorySuccess = todos => {
  return {
    type: FETCH_QUIZ_HISTORY_SUCCESS,
    payLoad: todos
  }
}

export const fetchQuizHistoryError = error => {
  return {
    type: FETCH_QUIZ_HISTORY_ERROR,
    payLoad: error
  }
}
