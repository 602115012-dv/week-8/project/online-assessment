import React from 'react';
import { connect } from 'react-redux';

const CoverImg = (props) => {
    const { quizDetail } = props;

    const coverStyle = {
        background: `url(${quizDetail.image})`,
        backgroundSize: "cover",
        height: 300,
        width: "100%"
    }

    return (
        <div style={coverStyle}></div>
    );
}

const mapStateToProps = (state) => {
    return {
        quizDetail: state.doingQuizPage.quizDetail
    };
}

export default connect(mapStateToProps)(CoverImg);