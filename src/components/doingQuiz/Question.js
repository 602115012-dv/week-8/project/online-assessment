import React from 'react';
import { Row, Col, Radio, Skeleton } from 'antd';
import './Question.css'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { selectAnswer } from '../../actions/DoingQuizPageAction';

const Question = (props) => {
    const { currentQuestion, questions, selectAnswer, answers, loadingQuizDetail } = props;

    const onChange = ({ target }) => {
        console.log('radio checked', target.value);
        selectAnswer(target.value);
    };

    return (
        <Row type="flex" justify="center">
            <Col span={20} className="containerStyle">
                {loadingQuizDetail ?
                    <Skeleton active />
                    :
                    questions[currentQuestion] != undefined ?
                        <div>
                            <p>{"(" + questions[currentQuestion].score + " points)"}</p>
                            <p className="question">{currentQuestion + 1}. {questions[currentQuestion].title}</p>
                            {
                                questions[currentQuestion].image != null ?
                                    <div style={{ textAlign: "center" }}>
                                        <img alt="questionImg" className="questionImg" src={questions[currentQuestion].image} />
                                    </div>
                                    :
                                    <div></div>
                            }
                            <div style={{ paddingLeft: 50 }}>
                                <Radio.Group
                                    onChange={onChange}
                                    size="large"
                                    value={answers[currentQuestion] != undefined ? answers[currentQuestion].value : -1}
                                >
                                    {
                                        questions[currentQuestion].answers.map((item, index) => {
                                            return (
                                                <Radio key={index} className="radioStyle" value={index}>
                                                    {item.value}
                                                </Radio>
                                            );
                                        })
                                    }
                                </Radio.Group>
                            </div>
                        </div>
                        :
                        <div></div>
                }
            </Col>
        </Row>
    );
}

const mapStateToProps = (state) => {
    return {
        currentQuestion: state.doingQuizPage.currentQuestion,
        questions: state.doingQuizPage.questions,
        answers: state.doingQuizPage.answers,
        loadingQuizDetail: state.doingQuizPage.loadingQuizDetail
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ selectAnswer }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Question);