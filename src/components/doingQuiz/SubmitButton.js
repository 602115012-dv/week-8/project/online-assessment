import React from 'react';
import axios from 'axios';
import { Button } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { resetDoingState, setIsUpdateUser, setIsUpdateQuiz, setSubmitClicked } from '../../actions/DoingQuizPageAction';
import { environment } from '../../constants/environment';

const SubmitButton = (props) => {
    const { questions, answers, quizDetail, user, isUpdatingUser, isUpdatingQuiz, setIsUpdateUser, setIsUpdateQuiz, setSubmitClicked } = props;

    const userId = user._id;
    const oldUserHistory = user.history;

    const submitStyle = {
        background: '#FF8C00',
        marginLeft: 10
    };

    const getTotalScore = (correctAnswer) => {
        let temp = 0;
        for (let i = 0; i < correctAnswer.length; i++) {
            temp += correctAnswer[i].score;
        }
        return temp;
    }

    const onSubmit = () => {
        setSubmitClicked(true);
        setIsUpdateUser(true);
        setIsUpdateQuiz(true);
        var tempCorrectAnswer = answers.filter(item => item.choice.isCorrectAnswer == true);
        updateUserHistory(tempCorrectAnswer);
        updateQuizUserScoreList(tempCorrectAnswer);
    }

    const updateUserHistory = (tempCorrectAnswer) => {
        var tempHistory = oldUserHistory.filter(item => item.quiz != quizDetail._id);
        const newUserHistory = [
            {
                propName: "history",
                value: [
                    ...tempHistory,
                    {
                        quiz: quizDetail._id,
                        score: getTotalScore(tempCorrectAnswer)
                    }
                ]
            }
        ];
        axios.patch(environment.usersApi + userId, newUserHistory)
            .then(res => {
                console.log(res);
                setTimeout(() => {
                    setIsUpdateUser(false);
                }, 1000);
            })
            .catch(error => console.log(error));
    }

    const updateQuizUserScoreList = (tempCorrectAnswer) => {
        var tempUserScoreList = quizDetail.userScoreList.filter(item => item.user._id != userId);
        const newQuizUserScoreList = [
            {
                propName: "userScoreList",
                value: [
                    ...tempUserScoreList,
                    {
                        user: userId,
                        score: getTotalScore(tempCorrectAnswer)
                    }
                ]
            }
        ];
        axios.patch(environment.quizzesApi + quizDetail._id, newQuizUserScoreList)
            .then(res => {
                console.log(res);
                setTimeout(() => {
                    setIsUpdateQuiz(false);
                }, 1000);
            })
            .catch(error => console.log(error));
    }

    return (
        <Button
            shape="round"
            type="primary"
            size="large"
            style={answers.length == questions.length ? submitStyle : { display: "none" }}
            onClick={onSubmit}
            loading={isUpdatingQuiz || isUpdatingUser}
        >
            Submit
        </Button>
    );
}

const mapStateToProps = (state) => {
    return {
        questions: state.doingQuizPage.questions,
        answers: state.doingQuizPage.answers,
        quizDetail: state.doingQuizPage.quizDetail,
        user: state.checklogin.user,
        isUpdatingUser: state.doingQuizPage.isUpdatingUser,
        isUpdatingQuiz: state.doingQuizPage.isUpdatingQuiz
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ resetDoingState, setIsUpdateUser, setIsUpdateQuiz, setSubmitClicked }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(SubmitButton);