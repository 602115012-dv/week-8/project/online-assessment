import React from 'react';
import { Row, Col, Button, Progress } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { nextQuestion, previousQuestion } from '../../actions/DoingQuizPageAction';
import SubmitButton from './SubmitButton';

const QuizProgress = (props) => {
    const { currentQuestion, questions, answers, nextQuestion, previousQuestion, quiz_id } = props;

    const buttonStyle = {
        background: '#FF8C00'
    };

    const getPercentage = () => {
        return ((currentQuestion + 1) / questions.length) * 100;
    };

    return (
        <Row type="flex" justify="center">
            <Col span={20} style={{ margin: '30px 0px 50px 0px' }}>
                <Row type="flex" align="middle" justify="space-between">
                    <Col>
                        <Button
                            shape="round"
                            type="primary"
                            size="large"
                            style={currentQuestion == 0 ? { visibility: "hidden" } : buttonStyle}
                            onClick={() => previousQuestion()}
                        >
                            Previous
                        </Button>
                    </Col>
                    <Col span={16}>
                        <Progress
                            strokeColor={{
                                '0%': '#36C47F',
                                '100%': '#87d068',
                            }}
                            percent={getPercentage()}
                            status="active"
                            format={() => <span style={{ color: '#FFFFFF' }}>{currentQuestion + 1} / {questions.length}</span>} />
                    </Col>
                    <Col>
                        <Button
                            shape="round"
                            type="primary"
                            size="large"
                            style={currentQuestion == questions.length - 1 || answers[currentQuestion] == undefined ? { visibility: "hidden" } : buttonStyle}
                            onClick={() => nextQuestion()}
                        >
                            Next
                        </Button>
                        <SubmitButton quiz_id={quiz_id} />
                    </Col>
                </Row>
            </Col>
        </Row >
    );
}

const mapStateToProps = (state) => {
    return {
        currentQuestion: state.doingQuizPage.currentQuestion,
        questions: state.doingQuizPage.questions,
        answers: state.doingQuizPage.answers
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ nextQuestion, previousQuestion }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(QuizProgress);
