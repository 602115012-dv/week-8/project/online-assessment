import React, { useEffect } from 'react';
import CoverImg from './CoverImg';
import Title from './Title';
import Question from './Question'
import QuizProgress from './QuizProgress';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchQuizDetail, resetDoingState, setSubmitClicked } from '../../actions/DoingQuizPageAction';
import HeaderPage from '../HeaderPage';
import { useHistory } from 'react-router';

const DoingQuizPage = (props) => {
    const quiz_id = props.match.params.quiz_id
    const { isUpdatingUser, isUpdatingQuiz, submitClicked, fetchQuizDetail, resetDoingState, setSubmitClicked } = props
    const history = useHistory();

    useEffect(() => {
        if (!submitClicked) {
            resetDoingState();
            fetchQuizDetail(quiz_id);
        } else {
            if (!isUpdatingUser && !isUpdatingQuiz) {
                setSubmitClicked(false);
                history.push("/result/" + quiz_id);
            }
        }
    }, [isUpdatingUser, isUpdatingQuiz, submitClicked]);

    return (
        <div>
            <HeaderPage />
            <div style={{ paddingTop: 66, minHeight: "100vh", background: "#2471A3" }}>
                <CoverImg />
                <Title />
                <Question />
                <QuizProgress quiz_id={quiz_id} />
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        isUpdatingUser: state.doingQuizPage.isUpdatingUser,
        isUpdatingQuiz: state.doingQuizPage.isUpdatingQuiz,
        submitClicked: state.doingQuizPage.submitClicked
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ fetchQuizDetail, resetDoingState, setSubmitClicked }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DoingQuizPage);
