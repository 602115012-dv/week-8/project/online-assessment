import React from 'react';
import { Row, Col } from 'antd';
import { connect } from 'react-redux';

const Title = (props) => {
    const { quizDetail } = props;

    const titleContainerStyle = {
        textAlign: "center",
        background: "#F39C12",
        boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
        borderRadius: "10px",
        marginTop: "50px",
        padding: "20px 0px"
    }

    const titleTextStyle = {
        fontWeight: "bold",
        fontSize: "30px",
        color: "#FFFFFF"
    }

    return (
        <Row type="flex" justify="center">
            <Col span={20} style={titleContainerStyle}>
                <span style={titleTextStyle}>{quizDetail.title}</span>
            </Col>
        </Row>
    );
}

const mapStateToProps = (state) => {
    return {
        quizDetail: state.doingQuizPage.quizDetail
    };
}

export default connect(mapStateToProps)(Title);
