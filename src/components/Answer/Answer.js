import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchQuizzes } from '../../actions/AnswerAction';
import TitleAns from './TitleAns';
import AnsList from './AnsList';

const AnswerPage = (props) => {
  const { fetchQuizzes } = props;

  useEffect(() => {
    fetchQuizzes(props.match.params.quiz_id);
  }, [])

  return (
    <div style={{
      backgroundColor: '#AFB42B',
      minHeight: '100vh'
    }}>
      <TitleAns />
      <AnsList />
    </div>
  );
}

const mapStateToProps = state => {
  return {
    questions: state.doingQuizPage.questions,
    answers: state.doingQuizPage.answers
  };
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ fetchQuizzes, }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AnswerPage);