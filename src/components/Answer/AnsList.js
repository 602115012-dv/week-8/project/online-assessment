import React from 'react';
import { Card, Col, Row, List, Tag } from 'antd';
import { connect } from 'react-redux';

const AnsList = (props) => {

  const { questions, answers } = props;
  const choices = ['a', 'b', 'c', 'd'];

  return (
    <div>
      <List
        grid={{ gutter: 16, column: 1 }}
        dataSource={questions}
        renderItem={(question, indexQuestion) => (
          <List.Item>
            <Row type='flex' justify='center'>
              <Col span={20}>
                <Card
                  title={
                    <Row type='flex' justify='center'>
                      <Col span={24}>
                        {(indexQuestion + 1) + '. ' + question.title}
                      </Col>
                      <Col span={24}>
                        <img src={question.image} />
                      </Col>
                    </Row>
                  }
                  style={{ padding: 20, borderRadius: 10 }}>
                  <div style={{ paddingTop: 20 }}>
                    {
                      question.answers.map((item, index) => {
                        return <div key={index} style={{ paddingLeft: 50 }}>
                          {
                            answers[indexQuestion] != undefined ?
                              item.isCorrectAnswer == true && answers[indexQuestion].value == index ?
                                <p style={{ color: 'green' }}>{choices[index] + ')'} {item.value}</p>
                                :
                                item.isCorrectAnswer == true ?
                                  <p style={{ color: 'green' }}>{choices[index] + ')'} {item.value}</p>
                                  :
                                  answers[indexQuestion].value == index ?
                                    <p style={{ color: 'red' }}>{choices[index] + ')'} {item.value}</p>
                                    :
                                    <p style={{ color: 'black' }}>{choices[index] + ')'} {item.value}</p>
                              :
                              item.isCorrectAnswer == true ?
                                <p style={{ color: 'green' }}>{choices[index] + ')'} {item.value}</p>
                                :
                                <p style={{ color: 'red' }}>{choices[index] + ')'} {item.value}</p>
                          }
                        </div>
                      })
                    }
                  </div>
                </Card>
              </Col>
            </Row>
          </List.Item>
        )}
      />
    </div>
  );
}

const mapStateToProps = state => {
  return {
    questions: state.answerCompState.quizQuestion,
    answers: state.doingQuizPage.answers
  };
}

export default connect(mapStateToProps)(AnsList);