import React from 'react';
import {  Col, Row } from 'antd';
import { connect } from 'react-redux';
import HeaderPage from '../HeaderPage';

const TitleAns = (props) => {

  const { quiz } = props;

  return (
    <div>
      <HeaderPage />
      <Row type='flex' justify='center' style={{ paddingTop: 106 }}>
        <Col>
          <div style={{
            marginBottom: 50,
            width: '1198px',
            height: '100px',
            left: '373px',
            top: '99px',
            borderRadius: '10px',
            backgroundColor: '#F39C12',
            boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
          }}>
            <p style={{
              fontFamily: 'Verdana',
              fontWeight: 'bold',
              fontSize: '26px',
              padding: '2.5%',
              lineHeight: '42px',
              textAlign: 'center',
              color: '#FFFFFF'
            }}>
              {quiz.title} <b>“ ANSWER ”</b>
            </p>
          </div>
        </Col>
      </Row>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    quiz: state.answerCompState.quiz
  };
}

export default connect(mapStateToProps)(TitleAns);