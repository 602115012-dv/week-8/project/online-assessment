import React from 'react';
import 'antd/dist/antd.css';
import { connect } from 'react-redux';
import { Layout, Menu, Icon, Avatar, Button, Row, Col } from 'antd';
import { useHistory } from 'react-router';

const { Header } = Layout;

const HeaderPage = (props) => {
    const { user } = props;
    const history = useHistory();

    return (
        <Header style={{ height: '66px', position: 'fixed', zIndex: 1, width: '100%' }}>
            <Row type="flex" justify="space-between">
                <Col>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        style={{ lineHeight: '64px' }}
                    >
                        <Menu.Item key="1" onClick={() => {
                            history.push('/home');
                        }}>
                            <Icon type="home" theme="filled" />
                            Home
                        </Menu.Item>
                        <Menu.Item key="2" onClick={() => {
                            history.push('/history')
                        }}>
                            <Icon type="clock-circle" theme="filled" />
                            History
                        </Menu.Item>
                        <Menu.Item key="3" onClick={() => {
                            history.push('/userquiz')
                        }}>
                            <Icon type="smile" theme="filled" />
                            MyQuiz
                        </Menu.Item>
                    </Menu>
                </Col>
                <Col>
                    <span style={{ color: '#FFFFFF' }}>{user.username}</span>
                    <Avatar style={{ width: 50, height: 50, margin: "0px 20px 0px 10px" }} src={user.image} />
                    &nbsp; <Button type="danger" onClick={() => { window.location = "/processLogout"; }}>Log out</Button>
                </Col>
            </Row>
        </Header>
    );
}

const mapStateToProps = state => {
    return {
        user: state.checklogin.user
    }
}

export default connect(mapStateToProps)(HeaderPage);
