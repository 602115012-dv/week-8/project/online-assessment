import React, {  useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect, useSelector } from 'react-redux';
import { fetchQuizzes } from '../../actions/result/ResultAction';
import { Progress, Button } from 'antd';
import { useHistory } from 'react-router';

const UserScore = (props) => {
  const { userResult, quizFullScore } = props;
  const [myScore, setMyScore] = useState(0);
  const userLogin = useSelector(state => state.checklogin.user);
  const history = useHistory();

  const getPercent = () => {
    return (myScore / quizFullScore.fullScore) * 100;
  }

  const innerFormat = () => {
    const filterScore = userResult.filter(item => item.user._id == userLogin._id);
    const temp = filterScore[0] != undefined ? filterScore[0].score : '';
    setMyScore(temp);
    return (
      <div
        style={{
          fontSize: '14px',
          fontFamily: 'Verdana',
          color: '#000000',
          marginTop: '10%',
        }}
      >
        <h1>Your score</h1>
        <h1 style={{ fontSize: '74px' }}>{myScore}<span style={{ fontSize: '20px' }}> /{quizFullScore.fullScore}</span></h1>
      </div>
    );
  }

  return (
    <div>
        <div
          style={{
            width: '930px',
            textAlign: 'center',
            fontSize: '20px',
            fontFamily: 'Verdana',
            fontWeight: 'bold',
            margin: '0 auto',
            padding: ' 10px 0px 40px 0px',
            backgroundColor: '#FEFEFE',
            boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)'
          }}
        >
          <h1>Congratz!!</h1>
          <Progress type="circle" width={290} percent={getPercent()} format={() => innerFormat()} />
          <div>
            <Button
              size='large'
              style={{
                marginTop:10,
                width: 125,
                height: 50,
                fontSize: '15px',
                color: '#FFFFFF',
                backgroundColor: '#B63A67',
                border: '1px solid #B63A67'
              }}
              onClick={() => history.push('/answer/'+quizFullScore._id)}
            >
              View Answer
            </Button>
          </div>
        </div>
      </div >
  )
}

const mapStateToProps = state => {
  return {
    userResult: state.resultCompState.userResult,
    quizFullScore: state.resultCompState.quizFullScore
  }
}

const mapDisPatchToProps = dispatch => {
  return bindActionCreators({ fetchQuizzes }, dispatch)
}

export default connect(mapStateToProps, mapDisPatchToProps)(UserScore);
