import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchQuizzes } from '../../actions/result/ResultAction';
import HeaderPage from '../../components/HeaderPage';
import Ranking from './Ranking';
import UserScore from './UserScore';

const ResultPage = (props) => {

  const { fetchQuizzes } = props;

  useEffect(() => {
    fetchQuizzes(props.match.params.quiz_id);
  }, [])

  return (
    <div>
      <HeaderPage />
      <div
        style={{
          minHeight: '100vh',
          paddingTop: '106px',
          backgroundColor: '#EAEAEA',
        }}
      >
        <UserScore />
      <Ranking />
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    userResult: state.resultCompState.userResult,
    quizFullScore: state.resultCompState.quizFullScore
  }
}

const mapDisPatchToProps = dispatch => {
  return bindActionCreators({ fetchQuizzes }, dispatch)
}

export default connect(mapStateToProps, mapDisPatchToProps)(ResultPage);
