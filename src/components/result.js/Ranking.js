import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchQuizzes } from '../../actions/result/ResultAction';
import { Button, Row, Col, Modal, Rate } from 'antd';
import { useHistory } from 'react-router';

const Ranking = (props) => {
  const { userResult } = props;
  const history = useHistory();

  const [visible, setVisible] = useState(false);

  const handleCancel = e => {
    console.log(e);
    setVisible(false);
  };

  const showModal = () => {
    setVisible(true);
  }

  return (
    <div>
      <div
        style={{
          height: '665px',
          marginTop: '5%',
          backgroundColor: '#B63A67'
        }}
      >
        <h1 style={{
          color: '#FFFFFF',
          fontSize: '64px',
          textAlign: 'center',
          paddingTop: '25px',
        }}
        >
          Ranking
        </h1>
        <div>
          <div>
            <Row type="flex" justify="center" align="bottom">
              <Col>
                <div
                  style={{
                    backgroundColor: 'rgba(51, 43, 36, 0.7)',
                    height: '245px',
                    width: '229px',
                    color: 'white',
                    textAlign: 'center'
                  }}
                >
                  <Row type='flex' justify='center'>
                    <Col style={{ padding: '10px' }}>
                      <img src='/3rdplace.png' alt="icon" />
                    </Col>
                  </Row>
                  <div style={{ fontSize: '36px' }}>
                    {
                      userResult[2] != undefined ?
                        userResult[2].user.username
                        :
                        '-'
                    }
                  </div>
                  <div style={{ fontSize: '24px' }}>
                    {
                      userResult[2] != undefined ?
                        'Score: ' + userResult[2].score
                        :
                        'Score: -'
                    }
                  </div>
                </div>
              </Col>
              <Col>
                <div
                  style={{
                    backgroundColor: 'rgba(51, 43, 36, 0.9)',
                    height: '380px',
                    width: '293px',
                    color: 'white',
                    textAlign: 'center'
                  }}
                >
                  <Row type='flex' justify='center'>
                    <Col>
                      <img src='/image_5.png' alt="icon" />
                    </Col>
                  </Row>
                  <div style={{ fontSize: '36px' }}>
                    {
                      userResult[0] != undefined ?
                        userResult[0].user.username
                        :
                        '-'
                    }
                  </div>
                  <div style={{ fontSize: '24px' }}>
                    {
                      userResult[0] != undefined ?
                        'Score: ' + userResult[0].score
                        :
                        'Score: -'
                    }
                  </div>
                </div>
              </Col>
              <Col>
                <div
                  style={{
                    backgroundColor: 'rgba(51, 43, 36, 0.8)',
                    height: '314px',
                    width: '229px',
                    color: 'white',
                    textAlign: 'center'
                  }}
                >
                  <Row type='flex' justify='center'>
                    <Col style={{ padding: '10px' }}>
                      <img src='/2ndplace.png' alt="icon" />
                    </Col>
                  </Row>
                  <div style={{ fontSize: '36px' }}>
                    {
                      userResult[1] != undefined ?
                        userResult[1].user.username
                        :
                        '-'
                    }
                  </div>
                  <div style={{ fontSize: '24px' }}>
                    {
                      userResult[1] != undefined ?
                        'Score: ' + userResult[1].score
                        :
                        'Score: -'
                    }
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </div>

        <Row type="flex" justify="center">
          <Col style={{ marginTop: 30 }}>
            <Button
              size='large'
              style={{
                width: '120px',
                height: 60,
                fontSize: '24px',
                color: '#FFFFFF',
                backgroundColor: '#FF8C00',
                border: '1px solid #FF8C00'
              }}
              onClick={showModal}
            >
              <b>Done</b>
            </Button>
          </Col>
        </Row>

        <div>
          <Modal
            title="Rating Star 🌟"
            visible={visible}
            onOk={() => history.push('/home')}
            onCancel={handleCancel}
          >
            <p>Do you like this quiz?...</p>
            <Rate onChange={value => console.log(value)} />
          </Modal>
        </div>

      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    userResult: state.resultCompState.userResult,
  }
}

export default connect(mapStateToProps)(Ranking);
