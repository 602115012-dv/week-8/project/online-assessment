import React, { useEffect } from 'react';
import Header from './Header';
import WrappedQuizForm from './QuizForm';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import WrappedQuestionForm from './QuestionForm';
import { fetchQuizById } from '../../actions/editQuizActions';
import UpdateQuestionButton from './UpdateQuizButton';
import AddQuestionButton from '../EditQuiz/AddQuestionButton';
import { Row } from 'antd'
import HeaderPage from '../HeaderPage';
const EditQuiz = (props) => {
  const {
    editQuizReducer,
    fetchQuizById
  } = props;

  useEffect(() => {
    fetchQuizById(props.match.params.id);
  }, []);

  const quiz = editQuizReducer.quiz ? editQuizReducer.quiz : null;

  const renderQuestions = () => {
    let questions = [];
    let length = quiz ? quiz.questions.length : 0;

    for (let i = 0; i < length; i++) {
      questions.push(<WrappedQuestionForm key={i + 1} questionKey={i} />);
    }

    return questions;
  }

  return (
    <Row style={{backgroundColor: '#7D3C98',minHeight:'100vh'}}>
      <HeaderPage />
      <Header />
      <WrappedQuizForm props={props} />
      {renderQuestions()}
      <AddQuestionButton />
      <UpdateQuestionButton />
    </Row>
  );
}

const mapStateToProps = state => {
  return {
    editQuizReducer: state.editQuizReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      fetchQuizById
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(EditQuiz);