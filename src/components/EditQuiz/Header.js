import React from 'react';
import { Row, Col, Typography } from 'antd';

const { Title } = Typography;

const Header = () => {
  return (
    <Row type="flex" justify="center" style={{ margin: "20px 0px", marginTop: 100 }}>
      <Col span={12} style={{
        padding: "20px",
        textAlign: "center",
        backgroundColor: "#F39C12",
        borderRadius: "10px",
        boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)"
      }}>
        <h1 style={{ color: 'white', }}><b>Edit Quiz</b></h1>
      </Col>
    </Row>
  );
}

export default Header;