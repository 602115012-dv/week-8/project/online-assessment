import React from 'react';
import { Row, Col, Button } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const UpdateQuestionButton = (props) => {
  const { editQuizReducer } = props;

  return (
    <Row type="flex" justify="center" style={{ margin: "20px 0px" }}>
      <Col
        span={12}
        style={{ textAlign: "center" }}
      >
        {/* <Button type="primary" onClick={editQuizReducer.submitFormFunc}>Update</Button> */}
        <Button style={{ backgroundColor: '#E75252', borderColor: '#E75252' }} type="primary"
          onClick={editQuizReducer.submitFormFunc}>
          Update
          </Button>
      </Col>
    </Row>
  );
}

const mapStateToProps = state => {
  return {
    editQuizReducer: state.editQuizReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateQuestionButton);