import React from 'react';
import { Row, Col, Icon } from 'antd';
import {
  addQuestion
} from '../../actions/editQuizActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const AddQuestionButton = (props) => {
  const { addQuestion } = props;

  return (
    <Row type="flex" justify="center" style={{ margin: "20px 0px" }}>
      <Col
        span={12}
        style={{ textAlign: "center" }}
      >
        <a onClick={addQuestion}>
          <img style={{ width: '50px' }} src="https://cdn3.iconfinder.com/data/icons/universal-3-1/614/14_-_Add-512.png" />
        </a>
      </Col>
    </Row>
  );
}

const mapStateToProps = state => {
  return {
    editQuizReducer: state.editQuizReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      addQuestion
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AddQuestionButton);