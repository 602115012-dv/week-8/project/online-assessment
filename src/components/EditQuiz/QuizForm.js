import React, { useEffect } from 'react';
import { Row, Col, Form, Input, Button, Checkbox, Upload, Icon } from 'antd';
import QuizType from '../../constants/QuizType';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setQuizEditingSubmitFunction, setQuizEditingImage, setQuizEditingImageFile } from '../../actions/editQuizActions';
import axios from 'axios';
import { environment } from '../../constants/environment';
import {
  beginImageUploading,
  endImageUploading
} from '../../actions/createQuizActions';
const { TextArea } = Input;
const QuizForm = (props) => {
  const { getFieldDecorator } = props.form;
  const {
    editQuizReducer,
    setQuizEditingSubmitFunction,
    setQuizEditingImage,
    createQuizReducer,
    setQuizEditingImageFile
  } = props;

  const { history } = props.props;

  const quiz = editQuizReducer.quiz;

  const handleChange = info => {
    let fileList = [...info.fileList];

    fileList = fileList.slice(-1);
    if (fileList[0]) {
      fileList[0].thumbUrl = editQuizReducer.quiz.image;
    } else {
      setQuizEditingImage(null);
    }
    setQuizEditingImageFile(fileList);
  };

  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err && !createQuizReducer.isImageIsuploading) {
        const updateOps = [];

        updateOps.push({
          propName: 'image',
          value: editQuizReducer.quiz.image
        })

        updateOps.push({
          propName: 'questions',
          value: quiz.questions
        })

        let totalScore = 0;
        quiz.questions.forEach(question => {
          totalScore += parseInt(question.score);
        });

        console.log(totalScore)

        updateOps.push({
          propName: 'fullScore',
          value: totalScore
        })

        for (let key in values) {
          let updateOp = {};
          updateOp.propName = key;
          updateOp.value = values[key];
          updateOps.push(updateOp);
        }

        console.log(updateOps)
        axios.patch(`${environment.quizzesApi}/${quiz._id}`, updateOps)
          .then(res => {
            console.log(res);
            alert('Update quiz succeed!');
            history.push('/userquiz');
          })
          .catch(err => console.log(err));
      } else if (createQuizReducer.isImageIsuploading) {
        alert('Image is uploading. Cannot create the quiz yet.')
      }
    })
  }

  useEffect(() => {
    setQuizEditingSubmitFunction(handleSubmit);
    
    if (quiz ? quiz.image : false) {
      setQuizEditingImageFile([
        {
          uid: '-1',
          name: quiz ? quiz.image : null,
          status: 'done',
          url: quiz ? quiz.image : null,
          thumbUrl: quiz ? quiz.image : null
        }
      ]);
    } else {
      setQuizEditingImageFile([]);
    }
    
    // eslint-disable-next-line
  }, [quiz]);

  const renderQuizTypes = () => {
    let types = [];
    for (let type in QuizType) {
      let key = types.length + 1;
      types.push(
        <Col span={12} key={key}>
          <Checkbox
            value={QuizType[type]} >
            {QuizType[type]}
          </Checkbox>
        </Col>
      )
    }

    return types;
  }

  const customRequest = ({ onSuccess, onError, file }) => {
    beginImageUploading();
    let formData = new FormData();
    formData.append('image', file);
    axios.post(environment.imageApi, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }).then(res => {
      setQuizEditingImage(res.data.location)
      endImageUploading();
      onSuccess();
    }).catch(err => {
      console.log(err);
      endImageUploading();
      onError();
    });
  }

  return (
    <Row type="flex" justify="center" style={{ margin: "20px 0px" }}>
      <Col
        span={12}
        style={{ backgroundColor: "#3E3C3C", borderRadius: "5px", paddingTop: '25px' }}>
        <Form
          labelCol={{ span: 5 }}
          wrapperCol={{ span: 12 }}
        >
          <Form.Item
            label="Title"
          >
            {getFieldDecorator('title', {
              rules: [
                { required: true, message: 'Please input quiz title!' },
              ],
              initialValue: quiz ?
                quiz.title : null
            }
            )(
              <Input
                placeholder="Title"
              />
            )}
          </Form.Item>
          <Form.Item
            label="Description"
          >
            {getFieldDecorator('description', {
              rules: [
                { required: true, message: 'Please input description!' }
              ],
              initialValue: quiz ?
                quiz.description : null
            })(
              <TextArea
                placeholder="Description"
                autoSize={{ minRows: 2, maxRows: 3 }}
              />,
            )}
          </Form.Item>
          <Form.Item label="Types">
            {getFieldDecorator('types', {
              rules: [
                { required: true, message: 'Please check at least one!' }
              ],
              initialValue: quiz ? quiz.types : null
            })(
              <Checkbox.Group style={{ width: '100%' }}>
                <Row>
                  {renderQuizTypes()}
                </Row>
              </Checkbox.Group>,
            )}
          </Form.Item>
          <Form.Item label="Image">
            <Upload
              fileList={editQuizReducer ? editQuizReducer.quizImageFile : []}
              onChange={handleChange}
              customRequest={customRequest}
              listType="picture"
            >
              <Button>
                <Icon type="upload" /> Upload
                </Button>
            </Upload>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
}

const mapStateToProps = state => {
  return {
    editQuizReducer: state.editQuizReducer,
    createQuizReducer: state.createQuizReducer
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setQuizEditingSubmitFunction,
      setQuizEditingImage,
      setQuizEditingImageFile
    }, dispatch);
}

const WrappedQuizForm = Form.create({ name: 'quizForm' })(QuizForm);

export default connect(mapStateToProps, mapDispatchToProps)(WrappedQuizForm);