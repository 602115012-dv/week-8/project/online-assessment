import React from 'react';
import { Row, Col, Button } from 'antd';
import { connect } from 'react-redux';

const CreateQuizButton = (props) => {
  const {
    createQuizReducer
  } = props;
  return (
    <Row type="flex" justify="center" style={{ margin: "20px 0px" }}>
      <Col
        span={12}
        style={{ textAlign: "center" }}
      >
        <Button style={{ backgroundColor: '#E75252', borderColor: '#E75252' }} type="primary" 
        onClick={createQuizReducer.submitFunc}>
          Create
          </Button>
      </Col>
    </Row>
  );
}

const mapStateToProps = state => {
  return {
    createQuizReducer: state.createQuizReducer,
  }
}

export default connect(mapStateToProps)(CreateQuizButton);