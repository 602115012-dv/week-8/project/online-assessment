import React, { useEffect } from 'react';
import { Row, Col, Form, Input, Typography, Upload, Button, Icon, Switch } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  updateQuestion,
  updateQuestionImageFile,
  beginImageUploading,
  endImageUploading,
  updateQuestionImageUrl,
  addForm
} from '../../actions/createQuizActions';
import axios from 'axios';
import { environment } from '../../constants/environment';
import { deleteQuestion } from '../../actions/createQuizActions';

const { Title } = Typography;
const { TextArea } = Input;
const QuestionForm = (props) => {
  const { getFieldDecorator } = props.form;
  const {
    createQuizReducer,
    questionKey,
    updateQuestion,
    updateQuestionImageFile,
    beginImageUploading,
    endImageUploading,
    updateQuestionImageUrl,
    addForm,
    deleteQuestion
  } = props;

  useEffect(() => {
    addForm(props.form);
    // eslint-disable-next-line
  }, []);

  const handleChange = info => {
    let fileList = [...info.fileList];

    fileList = fileList.slice(-1);
    if (fileList[0]) {
      fileList[0].thumbUrl = createQuizReducer.questions[questionKey].image;
    }
    updateQuestionImageFile(questionKey, fileList);
  };

  const customRequest = ({ onSuccess, onError, file }) => {
    beginImageUploading();
    let formData = new FormData();
    formData.append('image', file);
    axios.post(environment.imageApi, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }).then(res => {
      // setQuizImageUrl(res.data.location);
      updateQuestionImageUrl(questionKey, res.data.location);
      console.log(res.data.location)
      endImageUploading();;
      onSuccess();
    }).catch(err => {
      console.log(err);
      endImageUploading();
      onError();
    });
  }

  const renderAnswers = () => {
    let answers = [];
    for (let i = 0; i < 4; i++) {
      answers.push(
        <Form.Item
          label={`Answer ${i + 1}`}
          labelCol={{ span: 7 }}
          wrapperCol={{ span: 16 }}
          key={i + 1}
        >
          <Row>
            <Col span={14}>
              {getFieldDecorator(`answers ${i + 1}`, {
                rules: [
                  { required: true, message: 'Please input question answers!' },
                ],
              })(
                <Input
                  placeholder="Please input answer..."
                />,
              )}
            </Col>
            <Col span={9} offset={1}>
              Correct answer:
              <Switch
                onChange={(checked) => updateCorrectAnswers(checked, i)}
                checked={
                  createQuizReducer.questions[questionKey] ? 
                  createQuizReducer.questions[questionKey].answers[i].isCorrectAnswer : false}
              />
            </Col>
          </Row>


        </Form.Item>
      );
    }

    return answers;
  }

  const updateCorrectAnswers = (checked, index) => {
    let propName = "answers.isCorrectAnswer"
    let answerIndex = index;
    let value = checked;
    const op = {
      propName,
      value,
      answerIndex
    }

    updateQuestion(questionKey, op);
  }

  return (
    <div>
      <Row
        type="flex"
        justify="center"
        style={{ margin: "20px 0px" }}
      >
        <Col
          span={12}
          style={{ backgroundColor: "#3E3C3C", borderRadius: "5px",  paddingBottom: '25px' }}>
          <Form
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 12 }}
          >
            <div style={{ fontSize: '2rem', textAlign: 'end', margin: '5px 15px 0 0' }}>
              <Button type="danger" onClick={() => deleteQuestion(questionKey)} >X</Button>
            </div>
            <Row>  
              <Col span={20} push={4} >

                <Form.Item
                  label="Title"
                >
                  {getFieldDecorator('title', {
                    rules: [
                      { required: true, message: 'Please input question title!' },
                    ],
                  })(
                    <TextArea
                      placeholder="Please input the question... "
                      autoSize={{ minRows: 2, maxRows: 5 }}
                    />,
                  )}
                </Form.Item>
                <Form.Item
                  label="Score"
                >
                  {getFieldDecorator('score', {
                    rules: [
                      { required: true, message: 'Please input question score!' },
                      { pattern: "^[1-9]\\d*$", message: 'Score must be an non zero integer!' }
                    ],
                  })(
                    <Input
                      placeholder="Please input the score... "
                    />,
                  )}
                </Form.Item>
                <Form.Item label="Image">
                  <Upload
                    fileList={createQuizReducer.answersImageFiles[questionKey]}
                    onChange={handleChange}
                    customRequest={customRequest}
                    listType="picture"
                  >
                    <Button>
                      <Icon type="upload" /> Upload
                </Button>
                  </Upload>
                </Form.Item>
              </Col>

              <Col span={4} pull={18}>
                <h2 level={2} style={{ textAlign: "center", color: 'white' }}>{`Question ${questionKey + 1}`}</h2>
              </Col>
            </Row>
            {renderAnswers()}
          </Form>
        </Col>
      </Row>
    </div>
  );
}

const handleValuesChange = (props, changedValues, allValues) => {
  const { questionKey, updateQuestion } = props;
  let propName, value, answerIndex;
  for (let key in changedValues) {
    if (key.includes("answer")) {
      propName = "answers.value"
      answerIndex = parseInt(key[key.length - 1]) - 1;
    } else {
      propName = key;
    }
    value = changedValues[key];
  }
  const op = {
    propName,
    value,
    answerIndex
  }
  updateQuestion(questionKey, op);
}

const mapStateToProps = state => {
  return {
    createQuizReducer: state.createQuizReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      updateQuestion,
      updateQuestionImageFile,
      beginImageUploading,
      endImageUploading,
      updateQuestionImageUrl,
      addForm,
      deleteQuestion
    }, dispatch);
}

const WrappedQuestionForm = Form.create(
  {
    name: 'questionForm',
    onValuesChange: handleValuesChange
  }
)(QuestionForm);

export default connect(mapStateToProps, mapDispatchToProps)(WrappedQuestionForm);