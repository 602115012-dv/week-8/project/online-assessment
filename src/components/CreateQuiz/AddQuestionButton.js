import React from 'react';
import { Row, Col, Icon, Button } from 'antd';
import {
  addQuestion
} from '../../actions/createQuizActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

const AddQuestionButton = (props) => {
  const { addQuestion } = props;

  return (
    <Row type="flex" justify="center" style={{ margin: "20px 0px" }}>
      <Col
        span={12}
        style={{ textAlign: "center" }}
      >
        {/* <Icon 
            type="plus-circle" 
            theme="twoTone"
            twoToneColor="#F39C12"
            onClick={addQuestion} 
            style={{ fontSize: "4rem" }} 
          /> */}
        <a onClick={addQuestion}>
          <img style={{ width: '50px' }} src="https://cdn3.iconfinder.com/data/icons/universal-3-1/614/14_-_Add-512.png" />
        </a>
      </Col>
    </Row>
  );
}

const mapStateToProps = state => {
  return {
    createQuizReducer: state.createQuizReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      addQuestion
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AddQuestionButton);