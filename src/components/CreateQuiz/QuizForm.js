import React, { useEffect } from 'react';
import { Row, Col, Form, Input, Button, Checkbox, Upload, Icon } from 'antd';
import axios from 'axios';
import {
  setQuizImageFile,
  setQuizImageUrl,
  beginImageUploading,
  endImageUploading,
  setQuizSubmitFunction
} from '../../actions/createQuizActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import QuizType from '../../constants/QuizType';
import { environment } from '../../constants/environment';

const { TextArea } = Input;

const QuizForm = (props) => {
  const { getFieldDecorator } = props.form;
  const {
    createQuizReducer,
    checklogin,
    setQuizImageFile,
    setQuizImageUrl,
    beginImageUploading,
    endImageUploading,
    setQuizSubmitFunction
  } = props;

  const { history } = props.props;

  useEffect(() => {
    setQuizSubmitFunction(handleSubmit);
  }, [createQuizReducer.quizImage, createQuizReducer.questions, createQuizReducer.isImageUploading, checklogin.user._id]);

  const handleSubmit = e => {
    e.preventDefault();
    let isAllQuestionHaveCorrectAnswer = true,
      isAllQuestionFieldsFilled = true

    createQuizReducer.questions.forEach(question => {
      let isAtleastOneTrue = false;
      question.answers.forEach(answer => {
        isAtleastOneTrue = isAtleastOneTrue || answer.isCorrectAnswer;
      });
      console.log(isAtleastOneTrue);
      if (!isAtleastOneTrue) {
        isAllQuestionHaveCorrectAnswer = false;
      }
    });

    createQuizReducer.forms.forEach(form => {
      form.validateFields((err, values) => {
        if (err) {
          isAllQuestionFieldsFilled = false;
        }
      })
    })

    props.form.validateFields((err, values) => {
      if (!err &&
        isAllQuestionHaveCorrectAnswer &&
        isAllQuestionFieldsFilled &&
        !createQuizReducer.isImageUploading) {

        console.log(createQuizReducer)

        const quiz = {
          user: checklogin.user._id, 
          title: values.title,
          description: values.description,
          image: createQuizReducer.quizImage,
          types: values.types,
          rating: 0,
          fullScore: calculateTotalScore(),
          questions: createQuizReducer.questions
        }
          axios.post(environment.quizzesApi, quiz)
          .then(res => {
            console.log(res);
            alert('Create quiz succeed!');
            history.push('/userquiz');
          })
          .catch(err => console.log(err));

      } else if (!isAllQuestionHaveCorrectAnswer) {
        alert('ALl question must have a correct answer.')
      } else if (!isAllQuestionFieldsFilled) {
        alert('All question fields must be filled.')
      } else if (createQuizReducer.isImageUploading) {
        alert('Image is uploading. Cannot create the quiz yet.')
      }
    });
  }

  const handleChange = info => {
    let fileList = [...info.fileList];

    fileList = fileList.slice(-1);
    if (fileList[0]) {
      fileList[0].thumbUrl = createQuizReducer.quizImage;
    }
    setQuizImageFile(fileList);
  };

  const calculateTotalScore = () => {
    let sum = 0;
    createQuizReducer.questions.forEach(question => {
      console.log(question);
      sum += parseInt(question.score);
    })

    return sum;
  }

  const customRequest = ({ onSuccess, onError, file }) => {
    beginImageUploading();
    let formData = new FormData();
    formData.append('image', file);
    axios.post(environment.imageApi, formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }).then(res => {
      setQuizImageUrl(res.data.location);
      endImageUploading();
      onSuccess();
    }).catch(err => {
      console.log(err);
      endImageUploading();
      onError();
    });
  }

  const renderQuizTypes = () => {
    let types = [];
    for (let type in QuizType) {
      let key = types.length + 1;
      types.push(
        <Col span={12} key={key}>
          <Checkbox value={QuizType[type]}>{QuizType[type]}</Checkbox>
        </Col>
      )
    }

    return types;
  }

  return (
    <Row type="flex" justify="center" style={{ margin: "20px 0px" }}>
      <Col
        span={12}
        style={{ backgroundColor: "#3E3C3C", borderRadius: "5px", paddingTop: '25px', paddingBottom: '25px' }}>
        <Form
          onSubmit={handleSubmit}
          labelCol={{ span: 5 }}
          wrapperCol={{ span: 12 }}
          style={{ color: 'white' }}
        >
          <Form.Item
            label="Title"
          
          >
            {getFieldDecorator('title', {
              rules: [
                { required: true, message: 'Please input quiz title!' },
              ],
            })(
              <Input
                placeholder="Title"
              />
            )}
          </Form.Item>
          <Form.Item
            label="Description"
           
          >
            {getFieldDecorator('description', {
              rules: [
                { required: true, message: 'Please input description!' }
              ],
            })(
              <TextArea
                placeholder="Description"
                autoSize={{ minRows: 2, maxRows: 3 }}
              />,
            )}
          </Form.Item>
          <Form.Item label="Types">
            {getFieldDecorator('types', {
              rules: [
                { required: true, message: 'Please check at least one!' }
              ],
            })(
              <Checkbox.Group style={{ width: '100%' }}>
                <Row>
                  {renderQuizTypes()}
                </Row>
              </Checkbox.Group>,
            )}
          </Form.Item>
          <Form.Item label="Image">
            <Upload
              fileList={createQuizReducer.quizImageFile}
              onChange={handleChange}
              customRequest={customRequest}
              listType="picture"
            >
              <Button>
                <Icon type="upload" /> Upload
                </Button>
            </Upload>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
}

const mapStateToProps = state => {
  return {
    createQuizReducer: state.createQuizReducer,
    checklogin: state.checklogin
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      setQuizImageFile,
      setQuizImageUrl,
      beginImageUploading,
      endImageUploading,
      setQuizSubmitFunction
    }, dispatch);
}

const WrappedQuizForm = Form.create({ name: 'quizForm' })(QuizForm);

export default connect(mapStateToProps, mapDispatchToProps)(WrappedQuizForm);