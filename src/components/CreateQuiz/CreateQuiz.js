import React from 'react';
import './style.css';
import WrappedQuizForm from './QuizForm';
import Header from './Header';
import WrappedQuestionForm from './QuestionForm';
import AddQuestionButton from './AddQuestionButton';
import { connect } from 'react-redux';
import CreateQuizButton from './CreateQuizButton';
import { Row } from 'antd';
import HeaderPage from '../HeaderPage';
const CreateQuiz = (props) => {
  const { 
    createQuizReducer,
  } = props;

  const renderQuestionForms = () => {
    let questionForms = [];
    console.log()
    for (let i = 0; i < createQuizReducer.questionSize; i++) {
      questionForms.push(<WrappedQuestionForm key={i + 1} questionKey={i}  />);
    }

    return questionForms;
  }

  return (
    <Row style={{backgroundColor: '#7D3C98',minHeight:'100vh'}}>
      <HeaderPage />
      <Header />
      <WrappedQuizForm props={props} />
      {renderQuestionForms()}
      <AddQuestionButton />
      <CreateQuizButton />
    </Row>
  );
}

const mapStateToProps = state => {
  return {
    createQuizReducer: state.createQuizReducer,
  }
}


export default connect(mapStateToProps) (CreateQuiz);