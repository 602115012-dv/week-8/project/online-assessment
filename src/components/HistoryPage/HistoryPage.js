import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchUserHistory, fetchQuizzes } from '../../actions/history/HistoryAction';
import { Table, Tag, Col, Row } from 'antd';
import HeaderPage from '../../components/HeaderPage';

const HistoryPage = (props) => {
    const { userHistory, quizzes } = props;
    const { fetchUserHistory, fetchQuizzes, user } = props;

    useEffect(() => {
        fetchUserHistory(user._id);
        fetchQuizzes();
    }, [])

    const getQuizById = id => {
        var temp = quizzes.filter(item => item._id == id)
        console.log(temp[0])
        return temp[0];
    }

    const columns = [
        {
            title: 'Game',
            dataIndex: 'quiz',
            key: 'quiz',
            render: quizId => (
                <div>
                    {
                        getQuizById(quizId) != undefined ?
                            getQuizById(quizId).title
                            :
                            <div></div>
                    }
                </div>
            )
        },
        {
            title: 'Types',
            key: 'quiz',
            dataIndex: 'quiz',
            render: quizId => (
                <span>
                    {
                        getQuizById(quizId) != undefined ?
                            getQuizById(quizId).types.map(types => {
                                let color = 'magenta';
                                if (types === 'Science') {
                                    color = 'gold';
                                } else if (types === 'Biology') {
                                    color = 'green';
                                } else if (types === 'Chemistry') {
                                    color = 'volcano';
                                } else if (types === 'Physics') {
                                    color = 'geekblue';
                                } else if (types === 'Math') {
                                    color = 'cyan';
                                } else if (types === 'Cartoon') {
                                    color = 'orange';
                                } else if (types === 'Movie') {
                                    color = 'purple';
                                } else if (types === 'History') {
                                    color = 'red';
                                }
                                return (
                                    <Tag color={color} key={types}>
                                        {types.toUpperCase()}
                                    </Tag>
                                );
                            })
                            :
                            <div></div>
                    }
                </span>
            ),
        },
        {
            title: 'Score',
            dataIndex: 'score',
            key: 'score',
        },
        {
            title: 'FullScore',
            dataIndex: 'quiz',
            key: 'quiz',
            render: quizId => (
                <div>
                    {
                        getQuizById(quizId) != undefined ?
                            getQuizById(quizId).fullScore
                            :
                            <div></div>
                    }
                </div>
            )
        },

    ];

    return (
        <div>
            <HeaderPage />
            <div
                style={{
                    backgroundColor: '#CB4335',
                    paddingTop: '106px',
                    minHeight: '100vh'
                }}
            >
                <Row type="flex" justify="center">
                    <Col span={20}>
                        <div
                            style={{
                                borderRadius: '10px',
                                backgroundColor: '#F39C12',
                                boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
                            }}
                        >
                            <p
                                style={{
                                    fontFamily: 'Verdana',
                                    fontWeight: 'bold',
                                    fontSize: '26px',
                                    padding: '2.5%',
                                    lineHeight: '42px',
                                    textAlign: 'center',
                                    color: '#FFFFFF'
                                }}
                            >
                                Quiz History
                            </p>
                        </div>
                    </Col>
                    <Col span={20}>
                        <Table
                            style={{
                                backgroundColor: '#FFFFFF',
                                borderRadius: '10px'
                            }}

                            dataSource={userHistory}

                            columns={columns}
                        />
                    </Col>
                </Row>
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        userHistory: state.historyCompState.userHistory,
        quizzes: state.historyCompState.quizzes,
        user: state.checklogin.user
    }
}

const mapDisPatchToProps = (dispatch) => {
    return bindActionCreators({ fetchUserHistory, fetchQuizzes }, dispatch)
}

export default connect(mapStateToProps, mapDisPatchToProps)(HistoryPage);
