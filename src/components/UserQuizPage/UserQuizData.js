
import React, { useEffect } from 'react';
import '../../App.css';
import { fetchUserQuiz, deleteUserQuiz } from '../../actions/UserQuizAction';
import { connect } from 'react-redux';
//ant design 
import { Card, Row, Col, Rate, Button, Modal, Empty } from 'antd';
import { useHistory } from 'react-router-dom';
import { bindActionCreators } from 'redux';

const { confirm } = Modal;

const UserQuizData = (props) => {
    const history = useHistory();
    const { userQuizs, search, user } = props;
    const { fetchUserQuiz, deleteUserQuiz } = props;

    const userId = user._id;
    useEffect(() => {
        fetchUserQuiz(userId);
    }, [])

    const isDeleteQuiz = (id, index) => {
        confirm({
            title: 'Delete',
            content: 'Do you Want to delete these quiz?',
            onOk() {
                console.log('OK');
                // alert(index)
                deleteUserQuiz(id, index);
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }

    // let renderItems = userQuizs;
    var renderItems = search ?
        userQuizs.filter(m => m.title.match(search))
        :
        userQuizs

    return (
        <div>
            {

                renderItems.length == 0 ?
                    <Row
                        type="flex"
                        justify="center"
                        style={{ marginTop: '4em', backgroundColor: '#FEF9E7', padding: '50px 0 50px' }}>
                        <Col span={20}>
                            <Empty />
                        </Col>
                    </Row>
                    :
                    renderItems.map((item, index) => {
                        return (
                            <Card hoverable style={{ padding: 0, marginTop: '2em', borderRadius: '10px' }} key={index}>
                                <Row>
                                    <Col span={9}>
                                        <img src={item.image} style={{ width: '100%', height: '250px', borderRadius: '10px 0px 0px 10px' }} alt="img" />
                                    </Col>
                                    <Col span={15} style={{ padding: 20 }}>
                                        <h4> {item.title}</h4>
                                        <p> {item.description}</p>
                                        <p> Type : {item.types + " ,"}</p>
                                        <p> Full Score : {item.fullScore}</p>
                                        <span>Rating : </span>
                                        <Rate disabled defaultValue={item.rating} />

                                        <div>
                                            <Button style={{ backgroundColor: '#F39C12', border: ' #F39C12', marginTop: 10 }}
                                                type="primary"
                                                onClick={() => history.push("/doingQuiz/" + item._id)} >Start</Button>&nbsp;
                                            <Button type="primary" style={{ backgroundColor: '#EC7063', border: ' #EC7063', marginTop: 10 }}
                                                onClick={() => history.push('/answer/' + item._id)} >Answer</Button>

                                            <div style={{ float: 'right', paddingRight: '10px' }}>
                                                <Button type="dashed" shape="circle" icon="edit" onClick={() => history.push("/quiz/edit/" + item._id)} /> &nbsp;
                                            <Button type="dashed" shape="circle" icon="delete" value={index} onClick={() => isDeleteQuiz(item._id, index)} />
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                            </Card>
                        )
                    })
            }
        </div>
    )
}

const mapStateToProps = state => {
    return {
        userQuizs: state.userQuizList.userquizs,
        search: state.search,
        user: state.checklogin.user
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ fetchUserQuiz, deleteUserQuiz }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserQuizData);
