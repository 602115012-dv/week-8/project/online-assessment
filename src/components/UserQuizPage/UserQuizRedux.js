import React from 'react';
import 'antd/dist/antd.css';
import { Row, Col, Button } from 'antd';
import SearchForm from './SearchForm';
import UserQuizData from './UserQuizData';
import HeaderPage from '../HeaderPage';
import { useHistory } from 'react-router';

const UserQuizReduc = () => {
    const history = useHistory();

    const titleContainerStyle = {
        boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
        border: ' #1E8449',
        borderRadius: '10px',
        padding: '1.5em',
        textAlign: 'center',
        marginTop: '4em',
        backgroundColor: '#F39C12'
    }

    return (
        <div>
            <HeaderPage />
            <Row style={{
                backgroundColor: '#1E8449',
                paddingBottom: 100, minHeight: '100vh',
                paddingTop: '4em'

            }}>
                <Row type='flex'
                    justify='center'>
                    <Col span={4}></Col>
                    <Col span={16} style={titleContainerStyle} >
                        <h1 style={{ color: 'white', }}><b>My Quiz</b></h1>
                    </Col>
                    <Col span={4}>
                        <Button style={{ marginLeft: '10px', marginTop: '4em ', backgroundColor: '#FFD494' }} icon="plus"
                            onClick={() => history.push('/quiz/create')}><b> Create new Quiz</b></Button>
                    </Col>
                </Row>

                <Row type='flex'
                    justify='center'>
                    <Col >
                        <SearchForm />
                    </Col>
                </Row>

                <Row type='flex'
                    justify='center'>
                    <Col span={16}>
                        <UserQuizData />
                    </Col>
                </Row>

            </Row>
        </div>
    )
}

export default UserQuizReduc;
