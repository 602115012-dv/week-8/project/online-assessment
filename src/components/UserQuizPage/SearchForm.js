import React, { useState } from 'react';
import 'antd/dist/antd.css';
import { useDispatch } from 'react-redux';
import { searchData } from '../../actions/UserQuizAction';
import { Input } from 'antd';

const { Search } = Input;
const SearchForm = () => {
    const dispatch = useDispatch();

    const onSearch = event => {
        console.log(event.target.value)
        dispatch(searchData(event.target.value))
    }

    return (
        <Search
            placeholder="Please inut quiz name ..."
            onSearch={value => console.log(value)}
            style={{ width: 500, height: 35, marginTop: 30 }}
            onChange={onSearch} />
    )
}
export default SearchForm;