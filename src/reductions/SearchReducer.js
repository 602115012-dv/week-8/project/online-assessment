import { SEARCH_DATA } from '../constants/actionTypes';

const initialInput = '';
export const searchReducer = (state = initialInput, action) => {
    switch (action.type) {
        case SEARCH_DATA:
            return action.payLoad
        default:
            return state
    }
}