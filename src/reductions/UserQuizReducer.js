import {
    FETCH_USER_QUIZ_BEGIN, FETCH_USER_QUIZ_SUCCESS, FETCH_USER_QUIZ_ERROR, DELETE_BEGIN, DELETE_SUCCESS, DELETE_ERROR
} from '../constants/actionTypes';

const initialState = {
    userquizs: [],
    loading: false,
    error: ''
}

export const userQuizReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USER_QUIZ_BEGIN:
            return {
                ...state,
                loading: true
            }
        case FETCH_USER_QUIZ_SUCCESS:

            return {
                userquizs: action.payLoad,
                loading: false,
                error: ''
            }
        case FETCH_USER_QUIZ_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }


        case DELETE_BEGIN:
            // var temp = [...state.userquizs];
            // temp.splice(action.payLoad, 1);
            return {
                ...state,
                loading: true
            }

        case DELETE_SUCCESS:
            var temp = [...state.userquizs];
            temp.splice(action.payLoad, 1);
            return {
                ...state,
                userquizs: temp
            }

        case DELETE_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payLoad
            }
        default:
            return state
    }
}
