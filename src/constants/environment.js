const rootApi = 'http://52.14.93.246:8080';

export const environment = {
  imageApi: rootApi + '/image/',
  quizzesApi: rootApi + '/quizzes/',
  usersApi: rootApi + '/users/'
};