const QuizType = {
  SCIENCE: 'Science',
  MATH: 'Math',
  MOVIE: 'Movie',
  CARTOON: 'Cartoon',
  BIOLOGY: 'Biology',
  PHYSICS: 'Physics',
  CHEMISTRY: 'Chemistry',
  HISTORY: 'History'
}

export default QuizType;